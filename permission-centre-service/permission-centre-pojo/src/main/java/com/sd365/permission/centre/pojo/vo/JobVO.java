package com.sd365.permission.centre.pojo.vo;

import com.sd365.common.core.common.pojo.vo.BaseVO;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class JobVO extends BaseVO {
    /**
     * 任务名称
     */
    @ApiModelProperty(value = "jobName任务名称")
    private String jobName;
    /**
     *任务分组
     */
    @ApiModelProperty(value = "jobGroup任务分组")
    private String jobGroup;
    /**
     *任务描述
     */
    @ApiModelProperty(value = "description任务描述")
    private String description;
    /**
     *调用字符串
     */
    @ApiModelProperty(value = "targetPath调用字符串")
    private String targetPath;
    /**
     *cron表达式
     */
    @ApiModelProperty(value = "cron cron表达式")
    private String cron;
    /**
     * 上次运行时间
     */
    @ApiModelProperty(value = "lastRunTime上次运行时间")
    private Date lastRunTime;
    /**
     * 下次运行时间
     */
    @ApiModelProperty(value = "nextRunTime下次运行时间")
    private Date nextRunTime;
    /**
     *任务状态
     */
    @ApiModelProperty(value = "jobState任务状态")
    private String jobState;
}
