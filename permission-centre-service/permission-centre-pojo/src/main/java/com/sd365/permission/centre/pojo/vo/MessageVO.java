package com.sd365.permission.centre.pojo.vo;

import com.sd365.common.core.common.pojo.vo.BaseVO;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Table;

/**
 * 消息类
 *
 * @author 王海堂
 * @date 2022/07/161523
 **/
@Table(name = "basic_message")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MessageVO extends BaseVO {

    @ApiModelProperty(value = "messageType消息类型")
    private String messageType;

    @ApiModelProperty(value = "messageContent消息内容")
    private String messageContent;

    @ApiModelProperty(value = "pushTime推送间隔")
    private Long pushTime;

    @ApiModelProperty(value = "pushPath推送渠道")
    private String pushPath;

    @ApiModelProperty(value = "companyId公司id")
    private Long companyId;

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public String getMessageContent() {
        return messageContent;
    }

    public void setMessageContent(String messageContent) {
        this.messageContent = messageContent;
    }

    public Long getPushTime() {
        return pushTime;
    }

    public void setPushTime(Long pushTime) {
        this.pushTime = pushTime;
    }

    public String getPushPath() {
        return pushPath;
    }

    public void setPushPath(String pushPath) {
        this.pushPath = pushPath;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

}
