package com.sd365.permission.centre.pojo.query;

import com.sd365.common.core.common.pojo.vo.TenantBaseQuery;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author wujiandong
 * @date 2022/08/18
 * @description 参数管理 模糊查询
 * 解决 commonQuery 参数类型仅能通过id查询的问题
 */
@Data
@AllArgsConstructor
public class SystemParameterFuzzyQuery extends TenantBaseQuery {

    /**
     * 参数类型(字典类型)
     */
    private String paramType;

    /**
     * 参数项
     */
    private String param;

    /**
     * 参数值
     */
    private String value;
}
