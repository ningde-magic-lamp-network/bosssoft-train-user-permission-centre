package com.sd365.permission.centre.pojo.query;

import com.sd365.common.core.common.pojo.vo.BaseQuery;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Description:
 * @Author: WengYu
 * @CreateTime: 2022/06/24 11:53
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserOnlineQuery extends BaseQuery {
    /**
     * 名字
     */
    private String name;
    /**
     * 工号
     */
    private String code;
}
