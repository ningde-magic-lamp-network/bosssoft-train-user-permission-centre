package com.sd365.permission.centre.pojo.dto;
import com.sd365.common.core.common.pojo.dto.TenantBaseDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * @Class DictionaryDTO
 * @Description  字段参数
 * @Author Administrator
 * @Date 2023-02-22  11:47
 * @version 1.0.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class DictionaryDTO extends TenantBaseDTO {
    /**
     * 定义dictionaryTypeDTO
     */
    private DictionaryTypeDTO dictionaryTypeDTO=new DictionaryTypeDTO();
    /**
     * 所属公司，属于归属 租户 机构  公司 表中包含对应字段
     */
    private CompanyDTO companyDTO=new CompanyDTO();
    /**
     *  所属机构
     */
    private OrganizationDTO organizationDTO=new OrganizationDTO();
    /**
     *  字典表依赖字典类型表 所以有一个外键是字段类型id
     */

    private Long dictionaryTypeId;
    /**
     *  字典名称 不能重复
     */
    @NotNull(message = "字典名不能为空")
    private String name;
    /**
     *  字段值  不能重复
     */
    @NotNull(message = "字典值不能为空")
    private String value;
    /**
     *  备注 提供详细说明的预留字段
     */
    private String remark;

}
