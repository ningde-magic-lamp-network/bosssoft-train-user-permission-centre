package com.sd365.permission.centre.pojo.query;
import com.sd365.common.core.common.pojo.vo.TenantBaseQuery;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
/**
 * @Class RoleQuery
 * @Description  角色查询参数对应UI查询区域字段
 * @Author Administrator
 * @Date 2023-02-26  10:38
 * @version 1.0.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "角色管理界面查询区参数")
public class RoleQuery extends TenantBaseQuery {
    /**
     * 角色id
     */
    @ApiModelProperty(value="id角色id")
    private Long id;
    /**
     * 角色名
     */
    @ApiModelProperty(value="name角色名")
    private String name;

}
