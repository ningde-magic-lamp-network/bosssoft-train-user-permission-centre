package com.sd365.permission.centre.pojo.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Table;

/**
 * <p>加入公司名和机构名，用于传输数据到前端</p>
 * @author 周靖赟
 * @version 1.0
 * @since  12/12/20
 */
@Table(name = "basic_dictionary_category")
@Data
@EqualsAndHashCode(callSuper = true)
public class DictionaryCategoryVO extends com.sd365.common.core.common.pojo.vo.TenantBaseVO {
    private String name;
    private String remark;
}
