package com.sd365.permission.centre.pojo.query;


import com.sd365.common.core.common.pojo.vo.TenantBaseQuery;
import com.sd365.permission.centre.pojo.dto.DictionaryTypeDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.annotation.Transient;

@Data
@AllArgsConstructor
public class SystemParameterQuery extends TenantBaseQuery {
    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column system_parameter.dictionary_type_id
     *
     * @mbg.generated Tue Nov 03 17:42:34 CST 2020
     */
    private Long paramType;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column system_parameter.param
     *
     * @mbg.generated Tue Nov 03 17:42:34 CST 2020
     */
    private String param;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column system_parameter.value
     *
     * @mbg.generated Tue Nov 03 17:42:34 CST 2020
     */
    private String value;

    @Transient
    @ApiModelProperty(value="字典类型属性")
    private DictionaryTypeDTO dictionaryTypeDTO;

    public SystemParameterQuery() {
//        companyDTO = new CompanyDTO();
//        organizationDTO = new OrganizationDTO();
        dictionaryTypeDTO = new DictionaryTypeDTO();
    }
}
