package com.sd365.permission.centre.pojo.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.sd365.common.core.common.pojo.entity.TenantBaseEntity;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import java.util.Date;

@ApiModel(value="升级日志管理VO")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SysUpgradeLogVO extends TenantBaseEntity {

    @Transient
    private SubSystemVO subSystemVO;

    @Transient
    private TenantVO tenantVO;

    /**
     * 升级日志标题
     */
    private String title;

    /**
     * 升级日志内容
     */
    private String content;

    /**
     * 子系统id
     */
    private Long subSystemId;

    /**
     * 上一版本
     */
    private String oldVersion;

    /**
     * 这一版本
     */
    private String newVersion;

    /**
     * 更新时间
     */
    @NotNull
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss") //接收时间类型
    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss") //返回时间类型
    private Date upgradeTime;

    /**
     * 更新备注
     */
    private String upgradeRemark;

    /**
     * 升级操作人员
     */
    private String upgradeOperator;

}