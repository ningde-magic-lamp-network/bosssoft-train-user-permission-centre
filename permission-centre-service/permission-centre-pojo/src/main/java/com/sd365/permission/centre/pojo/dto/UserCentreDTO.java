package com.sd365.permission.centre.pojo.dto;

import com.sd365.common.core.common.pojo.dto.TenantBaseDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

/**
 * @author yb
 * @date 2022/12/051557
 **/
@ApiModel(value = "对外发布的用户DTO对象")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserCentreDTO extends TenantBaseDTO {
    /**
     * 角色用户所拥有的角色 可以通过 ResultMap <collection 指定子查询获取
     */
    @ApiModelProperty(value="角色列表")
    @Transient
    private List<RoleDTO> roleList;

    /**
     * 角色用户所拥有的部门 可以通过 ResultMap <assocation 指定子查询获取
     * 你也考可以考虑分开查询 或者 不带该字段
     */
    @ApiModelProperty(value="用户所属部门")
    @Transient
    private DepartmentDTO department;
    /**
     *  职位非关键字段例如 开发主管，开发经理等职位
     */
    @ApiModelProperty(value="职位")
    @Transient
    private PositionDTO position;
    /**
     * 工号
     */
    @NotNull(message = "code工号不能为空")
    @ApiModelProperty(value="code工号")
    private String code;

    /**
     * 密码
     */
    @ApiModelProperty(value="password密码")
    private String password;

    /**
     * 名字
     */
    @NotNull(message = "name名字不能为空")
    @ApiModelProperty(value="name名字")
    private String name;

    /**
     * 头像
     */
    @ApiModelProperty(value="profilePicture头像")
    private String profilePicture;

    /**
     * 性别
     */
    @NotNull(message = "sex性别不能为空")
    @ApiModelProperty(value="sex性别")
    private Byte sex;

    /**
     * 生日
     */
    @ApiModelProperty(value="birthday生日")
    private Date birthday;

    /**
     * 电话
     */
    @ApiModelProperty(value="tel电话")
    private String tel;

    /**
     * 邮箱
     */
    @ApiModelProperty(value="email邮箱")
    @NotNull(message = "email邮箱不能为空")
    private String email;

    /**
     * 其他联系
     */
    @ApiModelProperty(value="other其他联系")
    private String other;

    /**
     * 备注
     */
    @ApiModelProperty(value="remark备注")
    private String remark;

    /**
     * 部门ID
     */
    @ApiModelProperty(value="departmentId部门ID")
    private Long departmentId;

    /**
     * 职位ID  注意这个不等同于角色实际就是岗位
     */
    @ApiModelProperty(value="positionId职位ID  注意这个不等同于角色实际就是岗位")
    private Long positionId;

    /**
     * 公司
     */
    @Transient
    private CompanyDTO company;
    /**
     * 组织
     */
    @Transient
    private OrganizationDTO organization;
    /**
     * 组织
     */
    @Transient
    private TenantDTO tenant;

}
