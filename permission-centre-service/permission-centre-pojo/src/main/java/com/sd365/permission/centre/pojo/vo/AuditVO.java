package com.sd365.permission.centre.pojo.vo;

import com.sd365.common.core.common.pojo.dto.BaseDTO;
import com.sd365.common.core.common.pojo.vo.BaseVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Table;
import java.util.Date;

@ApiModel(value="com.sd365.permission.centre.entity.Audit")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "basic_audit")
public class AuditVO extends BaseVO {
    /**
     * 工号
     */
    @ApiModelProperty(value="工号")
    private String code;
    /**
     * 昵称
     */
    @ApiModelProperty(value="昵称")
    private String name;
    /**
     * 部门id
     */
    @ApiModelProperty(value="部门id")
    private Long departmentId;
    /**
     * 审计结果
     */
    @ApiModelProperty(value="审计结果")
    private String result;
    /**
     *使用简单密码
     */
    @ApiModelProperty(value="使用简单密码")
    private Byte useSimplePwd;
    /**
     * 定期未修改密码
     */
    @ApiModelProperty(value="定期未修改密码")
    private Byte noUpdatePwd;
    /**
     * 长期未登录
     */
    @ApiModelProperty(value="长期未登录")
    private Byte longNoLogin;
    /**
     * 密码修改时间
     */
    @ApiModelProperty(value="密码修改时间")
    private Date updatePwd;
    /**
     * 最后登录时间
     */
    @ApiModelProperty(value="最后登录时间")
    private Date endLogin;
}
