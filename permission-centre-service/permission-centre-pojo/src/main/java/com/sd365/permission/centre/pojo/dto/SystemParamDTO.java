package com.sd365.permission.centre.pojo.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.sd365.common.core.common.pojo.dto.TenantBaseDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.annotation.Transient;

@ApiModel(value="com.sd365.permission.centre.entity.SystemParamDTO")
@Data
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class SystemParamDTO extends TenantBaseDTO {
    /**
     * 字典类型_ID
     */
    @ApiModelProperty(value="paramType字典类型_ID")
    private Long paramType;

    /**
     * 参数项
     */
    @ApiModelProperty(value="param参数项")
    private String param;

    /**
     * 参数值
     */
    @ApiModelProperty(value="value参数值")
    private String value;

    /**
     * 版本号
     */
    private Long version;

    /**
     * 租赁人
     */
    private String tenantName;

    /**
     * 公司名称
     */
    private String companyName;

    /**
     * 机构名称
     */
    private String orgName;

    /**
     * 公司对象DTO
     */
    private CompanyDTO companyDTO;
    private String dictTypeName;
    /**
     * 机构对象DTO
     */
    private OrganizationDTO organizationDTO;

    /**
     * 字典类型属性DTO
     */
    @Transient
    @ApiModelProperty(value="字典类型属性")
    private DictionaryTypeDTO dictionaryTypeDTO;

    public SystemParamDTO() {
        companyDTO = new CompanyDTO();
        organizationDTO = new OrganizationDTO();
        dictionaryTypeDTO = new DictionaryTypeDTO();
    }
}
