package com.sd365.permission.centre.pojo.dto;

import com.sd365.common.core.common.pojo.dto.TenantBaseDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @Class RoleCompanyDTO
 * @Description 用于实现角色分配公司的数据结构
 * @Author Administrator
 * @Date 2023-04-20  10:47
 * @version 1.0.0
 */
@ApiModel(value = "角色公司管理")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RoleCompanyDTO extends TenantBaseDTO {
    /**
     * 角色ID
     */
    @ApiModelProperty(value = "roleIds待分配角色ID列表",required = true)
    @NotEmpty(message = "角色列表不能为空")
    private List<Long> roleIds;

    /**
     * 角色id列表
     */
    @ApiModelProperty(value = "authCompanyIds授权公司ID列表",required = true)
    @NotEmpty(message = "授权公司不能为空")
    private List<Long> authCompanyIds;
}
