/**
 * Copyright (C), 2001-2031, www.bosssof.com.cn
 * FileName: LoginUserDTO.java
 * Author: abel.zhan
 * Date: 2024-10-21 17:26
 * Description:
 * <p>
 * History:
 * Date          Author   Version  Desc
 * 2024-01-01    bosssoft  1.0.0   initialize this file
 */
package com.sd365.permission.centre.pojo.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 登录认证成功返回的用户数据对象
 *
 * <p>
 * 主要用于登录模块使用，返回给前端。
 * 该模块从 LoginService的内部类被重构到这里
 * </p>
 * </p>
 * @author Abel Zhan
 * @date 2024-09-03 14:14
 * @version 1.0.0
 * @since 1.0.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class LoginUserDTO{
    /**
     * 带回认证的错误码
     */
    private Integer resultCode;
    private Long id;
    private Long tenantId;
    private Long orgId;
    private Long companyId;
    private String name;
    private String tel;
    private List<Long> roleIds;

}
