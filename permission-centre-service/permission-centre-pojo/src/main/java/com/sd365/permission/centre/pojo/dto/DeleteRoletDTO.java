package com.sd365.permission.centre.pojo.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.sd365.common.core.common.pojo.dto.BaseDTO;
import com.sd365.common.core.common.pojo.dto.TenantBaseDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

/**
 * @Class DeleteRoletDTO
 * @Description
 * abel.zhan 2023.07.26 用于替换原来的 RoleApi的批量删除方法的参数，具体的参考方法修改代码
 * @Author Administrator
 * @Date 2023-07-26  9:39
 * @version 1.0.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class DeleteRoletDTO {
    /**
     * 前端多行选择的时候传过来后端不再雪花算法生成id
     */
    @NotNull(message = "id 不能为空")
    private Long id;
    /**
     * 防止多人同时修改使用行版本控制
     */
    @NotNull(message = "版本不能为空")
    private Long version;
}
