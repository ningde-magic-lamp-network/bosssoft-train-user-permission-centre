package com.sd365.permission.centre.pojo.query;


import com.sd365.common.core.common.pojo.vo.TenantBaseQuery;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * @ClassName UserQuery
 * @Description 用户的搜索
 * @Author yangshaoqi
 * @Date 2020/12/15 13:13
 * @Version 1.0
 **/
@ApiModel
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserQuery extends TenantBaseQuery {
    /**
     * 名字
     */
    @ApiModelProperty(value = "姓名，支持模糊查询")
    private String name;
    /**
     * 工号
     */
    @ApiModelProperty(value = "工号，支持模糊查询")
    private String code;
    /**
     * 电话
     */
    @ApiModelProperty(value = "电话")
    private String tel;
    /**
     * 部门
     */
    @ApiModelProperty(value = "部门id")
    private Long departmentId;
    /**
     * 职位
     */
    @ApiModelProperty(value = "职位id")
    private Long positionId;
    /**
     * 角色
     */
    @ApiModelProperty(value = "角色id")
    private Long roleId;


}
