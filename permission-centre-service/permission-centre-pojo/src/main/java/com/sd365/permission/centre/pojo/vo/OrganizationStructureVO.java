package com.sd365.permission.centre.pojo.vo;

import lombok.Data;

import java.util.List;

/**
 * @Version :1.0
 * @PROJECT_NAME: sd365-permission-centre
 * @PACKAGE_NAME:com.sd365.permission.centre.pojo.vo
 * @NAME: OrganizationStructureVO
 * @author:xuandian
 * @DATE: 2022/7/4 18:35
 * @description: 架构下的公司以及公司下的部门信息
 */
@Data
public class OrganizationStructureVO {
    private String label;
    private Long id;
    private List<OrganizationStructureVO> children;
}
