package com.sd365.permission.centre.pojo.dto;

import com.sd365.common.core.common.pojo.entity.TenantBaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data

@AllArgsConstructor
public class PositionDTO extends TenantBaseEntity {
    /**
     * 职位名称
     */
    @ApiModelProperty(value="name职位名称")
    private String name;

    /**
     * 职位编号
     */
    @ApiModelProperty(value="code职位编号")
    private String code;

    /**
     * 备注
     */
    @ApiModelProperty(value="remark备注")
    private String remark;
    private OrganizationDTO organizationDTO;
    private CompanyDTO companyDTO;


    public PositionDTO() {

        organizationDTO=new OrganizationDTO();
        companyDTO=new CompanyDTO();
    }
    /**
     * 职位名称
     */
    public String getName() {
        return name;
    }

    /**
     * 职位名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 职位编号
     */
    public String getCode() {
        return code;
    }

    /**
     * 职位编号
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * 备注
     */
    public String getRemark() {
        return remark;
    }

    /**
     * 备注
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }
}