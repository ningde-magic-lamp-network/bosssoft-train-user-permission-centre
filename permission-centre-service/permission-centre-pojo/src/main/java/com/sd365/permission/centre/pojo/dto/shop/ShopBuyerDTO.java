package com.sd365.permission.centre.pojo.dto.shop;

import com.sd365.common.core.common.pojo.dto.TenantBaseDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Table;

@ApiModel(value="com.sd365.smp.flower.dao.entity.ShopBuyer")
@Table(name = "shop_buyer")
@Data
public class ShopBuyerDTO extends TenantBaseDTO {
    private String wxOpenId;
    /**
     * 图片地址
     */
    @ApiModelProperty(value="imageUrl图片地址")
    private String imageUrl;

    /**
     * 电话
     */
    @ApiModelProperty(value="tel电话")
    private String tel;

    /**
     * 性别
     */
    @ApiModelProperty(value="sex性别")
    private Boolean sex;

    /**
     * 账户
     */
    @ApiModelProperty(value="account账户")
    private String account;

    /**
     * 姓名
     */
    @ApiModelProperty(value="name姓名")
    private String name;

    /**
     * 1普通用户，2vip
     */
    @ApiModelProperty(value="attribute1普通用户，2vip")
    private String attribute;

    /**
     * 
     */
    @ApiModelProperty(value="extend1")
    private String extend1;

    /**
     * 
     */
    @ApiModelProperty(value="extend2")
    private String extend2;

    /**
     * 
     */
    @ApiModelProperty(value="extend3")
    private String extend3;

    /**
     * 图片地址
     */
    public String getImageUrl() {
        return imageUrl;
    }

    /**
     * 图片地址
     */
    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    /**
     * 电话
     */
    public String getTel() {
        return tel;
    }

    /**
     * 电话
     */
    public void setTel(String tel) {
        this.tel = tel;
    }

    /**
     * 性别
     */
    public Boolean getSex() {
        return sex;
    }

    /**
     * 性别
     */
    public void setSex(Boolean sex) {
        this.sex = sex;
    }

    /**
     * 账户
     */
    public String getAccount() {
        return account;
    }

    /**
     * 账户
     */
    public void setAccount(String account) {
        this.account = account;
    }

    /**
     * 姓名
     */
    public String getName() {
        return name;
    }

    /**
     * 姓名
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 1普通用户，2vip
     */
    public String getAttribute() {
        return attribute;
    }

    /**
     * 1普通用户，2vip
     */
    public void setAttribute(String attribute) {
        this.attribute = attribute;
    }

    /**
     * 
     */
    public String getExtend1() {
        return extend1;
    }

    /**
     * 
     */
    public void setExtend1(String extend1) {
        this.extend1 = extend1;
    }

    /**
     * 
     */
    public String getExtend2() {
        return extend2;
    }

    /**
     * 
     */
    public void setExtend2(String extend2) {
        this.extend2 = extend2;
    }

    /**
     * 
     */
    public String getExtend3() {
        return extend3;
    }

    /**
     * 
     */
    public void setExtend3(String extend3) {
        this.extend3 = extend3;
    }
}