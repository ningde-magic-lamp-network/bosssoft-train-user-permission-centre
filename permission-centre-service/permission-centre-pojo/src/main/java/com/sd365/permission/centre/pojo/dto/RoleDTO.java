/**
 * Copyright (C), 2022-2025, www.bosssof.com.cn
 * @FileName RoleDTO.java
 * @Author Administrator
 * @Date 2022-9-28  18:08
 * @Description TODO
 * History:
 * <author> Administrator
 * <time> 2022-9-28  18:08
 * <version> 1.0.0
 * <desc> 该文件定义了前端对角色做增加和修改的时候传递后端的角色DTO对象
 */
package com.sd365.permission.centre.pojo.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.sd365.common.core.common.pojo.dto.TenantBaseDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
/**
 * @Class RoleDTO
 * @Description 前端做角色增加和修改的时候传送对象的角色对象
 * @Author Administrator
 * @Date 2022-9-28  18:09
 * @version 1.0.0
 */
@ApiModel(value="角色管理增加和修改传输对象")
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class RoleDTO extends TenantBaseDTO {
    /**
     *  角色所包含的资源列表
     */
    private List<ResourceDTO> resourceDTOS=new ArrayList<>();

    /**
     * 角色名
     */
    @ApiModelProperty(value="name角色名")
    @NotNull(message = "角色名字不能为空")
    private String name;
    /**
     * 角色代码
     */
    @ApiModelProperty(value="code角色代码")
    @NotNull(message = "角色工号不能为空")
    private String code;

    /**
     * 备注
     */
    @ApiModelProperty(value="remark备注")
    private String remark;

    /**
     *  机构
     */
    private OrganizationDTO organizationDTO=new OrganizationDTO();
    /**
     *  公司
     */
    private CompanyDTO companyDTO=new CompanyDTO();

    /**
     * 租户
     */
    private  TenantDTO tenantDTO=new TenantDTO();

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createdTime;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updatedTime;
}
