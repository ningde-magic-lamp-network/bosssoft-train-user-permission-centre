/**
 * @Version :1.0
 * @PROJECT_NAME: sd365-permission-centre
 * @PACKAGE_NAME:com.sd365.permission.centre.pojo.vo
 * @NAME: CacheInfoVO
 * @author:xuandian
 * @DATE: 2022/7/12 17:25
 * @description: 缓存监控主要信息
 */
package com.sd365.permission.centre.pojo.vo;
import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

/**
 * @Class CacheInfoVO
 * @Description 缓存的汇总信息，通过改信息了解缓存的总体情况
 * @Author Administrator
 * @Date 2023-02-20  21:50
 * @version 1.0.0
 */
@Data
public class CacheInfoVO implements Serializable {
    /**
     * 统计 keys
     */
    private List<KeyValueVo> keyVos;
    /**
     * redis 缓存的内容
     */
    private List<Map<Object, Object>> values;
    /**
     * 获取redis缓存完整信息
     */
    private Properties info;
    /**
     * 获取redis缓存命令统计信息
     */
    private List<OrderChcheVO> orderChcheVOS;
    /**
     * key 的数量
     */
    private Long dbSize;
}
