package com.sd365.permission.centre.pojo.vo;

import lombok.Data;

/**
 * @Version :1.2
 * @PROJECT_NAME: sd365-permission-centre
 * @PACKAGE_NAME:com.sd365.permission.centre.pojo.vo
 * @NAME: OrderVO
 * @author:xuandian
 * @DATE: 2022/7/13 14:31
 * @description:缓存指令
 */
@Data
public class OrderChcheVO {
    private String name;
    private String totalNum;
}
