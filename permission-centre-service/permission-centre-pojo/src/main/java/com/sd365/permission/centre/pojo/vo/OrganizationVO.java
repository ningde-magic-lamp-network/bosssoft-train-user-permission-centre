package com.sd365.permission.centre.pojo.vo;

import com.sd365.common.core.common.pojo.entity.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @className: OrganizationVO
 * @description: 组织VO
 * @author: 何浪
 * @date: 2020/12/11 17:03
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrganizationVO extends BaseEntity {
    /**
     * 名称
     */
    private String name;

    /**
     * 编号
     */
    private String code;

    /**
     * 负责人
     */
    private String master;

    /**
     * 手机号
     */
    private String tel;

    /**
     * 地址
     */
    private String address;
}

