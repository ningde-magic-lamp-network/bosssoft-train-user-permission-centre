package com.sd365.permission.centre.pojo.query;

import com.sd365.common.core.common.pojo.vo.TenantBaseQuery;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * <p>查询使用的类，用于自动分页</p>
 * @author 周靖赟
 * @version 1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString
public class DictionaryCategoryQuery extends TenantBaseQuery {
    /**
     * 名称
     */
    private String name;
    /**
     * 备注
     */
    private String remark;
}
