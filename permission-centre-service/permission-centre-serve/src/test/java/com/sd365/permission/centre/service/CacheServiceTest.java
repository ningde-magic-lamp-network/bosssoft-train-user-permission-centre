package com.sd365.permission.centre.service;

import com.sd365.permission.centre.pojo.vo.CacheInfoVO;
import com.sd365.permission.centre.pojo.vo.KeyValueVo;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @Version :
 * @PROJECT_NAME: sd365-permission-centre
 * @PACKAGE_NAME:com.sd365.permission.centre.service
 * @NAME: CacheServiceTest
 * @author:xuandian
 * @DATE: 2022/7/13 18:20
 * @description:
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
class CacheServiceTest {
    @Resource
    private CacheService cacheService;

    @Test
    void getCacheInfo() {
        //test 1
//        CacheInfoVO cacheInfo = cacheService.getCacheInfo(null);
//        log.info("{},", cacheInfo);
        //test 2
        String key = "1537404443832745984";
        CacheInfoVO cacheInfo = cacheService.getCacheInfo(key);
        log.info("{},", cacheInfo);
    }

    @Test
    void queryByKey() {
        String key = "1537404443832745984";
        KeyValueVo keyValueVo = cacheService.queryByKey(key);
        log.info("{},", keyValueVo);
    }

    @Test
    void deleteByKey() {
        String key = "1377523913285813807";
        boolean flag = cacheService.deleteByKey(key);
        log.info("{},", flag);
    }
}