package com.sd365.permission.centre.service.impl;

import com.sd365.common.core.common.exception.BusinessException;
import com.sd365.common.core.common.exception.code.CommonErrorCode;
import com.sd365.common.core.common.service.AbstractBusinessService;
import com.sd365.common.util.BeanUtil;
import com.sd365.permission.centre.dao.mapper.ResourceMapper;
import com.sd365.permission.centre.entity.Resource;
import com.sd365.permission.centre.entity.Role;
import com.sd365.permission.centre.pojo.vo.ResourceVO;
import com.sd365.permission.centre.service.MenuService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * @author No_one
 * @description 根据资源生成菜单
 */
@Slf4j
@Service
public class MenuServiceImpl extends AbstractBusinessService implements MenuService {
    @javax.annotation.Resource
    private ResourceMapper resourceMapper;

    @Override
    public List<ResourceVO>  buildMenu(@NotNull @NotEmpty List<Resource> resourceList) {
        List<ResourceVO> resourceVOS=new ArrayList<>();
        for(Resource resource : resourceList) {
            if(ObjectUtils.isEmpty(resource)){
                throw new BusinessException(CommonErrorCode.NULL_POINT_ERROR,new RuntimeException("资源列表存在空对象，请检查resourceList参数"));
            }
            if(resource.getParentId() == IS_PARENT) {
                resourceVOS.add(BeanUtil.copy(resource, ResourceVO.class));
            }
        }
        // 递归生成菜单
        resourceVOS.stream().forEach(resourceVO -> {
            recBuildMenu(resourceVO, resourceList);
        });
        return resourceVOS;
    }

    @Override
    public List<ResourceVO> getResourceVO(List<Role> roles) {
        /**
         * 根据角色取得所有的角色id加入列表
         */
        List<Long> roleIds = new ArrayList<>();
        roles.stream().forEach(role -> roleIds.add(role.getId()));
        // 批量查询取得角色所包含的所有的资源
        List<Resource> resources=resourceMapper.getResourceByRoleId(roleIds);
        /**
         * 取得角色对应的资源
         *  2022-10-12 重构了该方法增加注释已经保证不返回空对象
         * @param roles  当前用户的所有的角色
         * @return 所拥有的资源 如果角色没有资源方法返回 null
         */

        // 这个service按规范应该是dto出去的 这个代码有待优化 另外这个功能也很奇怪
        return buildMenu(resources);
    }

    /**
     *  递归菜单生成
     * @param resourceVO  菜单父亲节点
     * @param resourceList  所有的菜单列表
     */
    private void recBuildMenu(ResourceVO resourceVO, List<Resource> resourceList) {
        Long id = resourceVO.getId();
        List<ResourceVO> subList = new LinkedList<>();
        // 查找资源列表里的子资源
        // 检验是否查询结束，递归结束
        boolean flag = true;
        for(Resource resource : resourceList) {
            if(resource.getParentId().equals(id)) {
                flag = false;
                ResourceVO subResourceVO = null;
                // copy resource to subResourceVO
                subResourceVO = BeanUtil.copy(resource, ResourceVO.class);
                subList.add(subResourceVO);
            }
        }
        // 该资源没有子资源，递归结束
        if(flag) {
            resourceVO.setResourceVOs(null);
            return;
        }
        resourceVO.setResourceVOs(subList);
        for(ResourceVO resourceVO1 : subList) {
            recBuildMenu(resourceVO1, resourceList);
        }
    }
}
