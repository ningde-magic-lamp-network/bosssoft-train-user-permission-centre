/**
 * Copyright (C), 2001-2023, www.bosssof.com.cn
 * FileName: ResourceCache
 * Author: Administrator
 * Date: 2023-04-20 14:28:29
 * Description:
 * <p>
 * History:
 * <author> Administrator
 * <time> 2023-04-20 14:28:29
 * <version> 1.0.0
 * <desc> 版本描述
 */
package com.sd365.permission.centre.service.cache;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.sd365.permission.centre.dao.mapper.ResourceMapper;
import com.sd365.permission.centre.entity.Resource;
import com.sd365.permission.centre.pojo.query.ResourceQuery;
import com.sd365.permission.centre.service.config.Global;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import javax.validation.constraints.Null;
import java.util.List;

/**
 * @ClassName: ResourceCache
 * @Description:
 * 在执行 resource增删除改业务的时候通过该Cache工具维护redis
 * @Author Administrator
 * @Date: 2023-04-20 14:28
 **/
@Slf4j
@Component
public class ResourceCache implements IResourceCache{
    /**
     * 操作缓存的redis
     */
    @javax.annotation.Resource(name = "roleResourceRedisTemplate")
    private RedisTemplate<String,Object> redisTemplate;
    /**
     * 用于建立resource的缓存的查询
     */
    @Autowired
    private ResourceMapper resourceMapper;
    /**
     * 行记录 ROW_DATA_STATUS 插入就是 1  如果被停用就 0
     */
    private static final byte ROW_DATA_STATUS_ACTIVE=1;
    /**
     * 行记录 ROW_DATA_STATUS 插入就是 1  如果被停用就 0
     */
    private static final byte ROW_DATA_STATUS_INACTIVE=0;


    @Override
    public void updateCache(Resource resource) {
        redisTemplate.opsForValue().set(Global.RESOURCE_ID_KEY+String.valueOf(resource.getId()),resource);
    }

    @Override
    public void deleteCache(Long  id) {
        redisTemplate.delete(Global.RESOURCE_ID_KEY+String.valueOf(id));
    }

    /**
     * 初始化resource 到缓存，如果对resource做修改和删除这更新
     * @param resourceQuery
     */
    @Override
    public void cachedAllResources(ResourceQuery resourceQuery) {
        List<Resource> resources = resourceMapper.commonQuery(resourceQuery);
        resources.stream().forEach(resource->{
           if(resource.getStatus()!=ROW_DATA_STATUS_INACTIVE){
                redisTemplate.opsForValue().set(Global.RESOURCE_ID_KEY+String.valueOf(resource.getId()),
                        resource);
            }

        });
    }

    @Override
    public void add2Cache(Resource resource) {
        redisTemplate.opsForValue().set(Global.RESOURCE_ID_KEY+String.valueOf(resource.getId()),resource);
    }

    @Override
    public Resource getCacheResourceById(Long id) {
        Object resource=redisTemplate.opsForValue().get(Global.RESOURCE_ID_KEY+String.valueOf(id));
        // resource 为  linkHashMao 转化为 jsonString之后在转化为对象
        return null==resource ? null : JSON.parseObject(JSONObject.toJSONString(resource),Resource.class);
    }


}
