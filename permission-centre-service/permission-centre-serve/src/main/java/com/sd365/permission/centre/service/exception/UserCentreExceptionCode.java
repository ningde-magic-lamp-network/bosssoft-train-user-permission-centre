package com.sd365.permission.centre.service.exception;

import com.sd365.common.core.common.exception.code.IErrorCode;
/**
 * @Class UserCentreExceptionCode
 * @Description 根据系统的业务规则定义了业务类
 * @Author Administrator
 * @Date 2023-02-13  11:17
 * @version 1.0.0
 */
public enum UserCentreExceptionCode implements IErrorCode {

    /**
     * 模块操作异常
     */
    BUSINESS_ROLE_RULE_NOT_ALLOW_DELETE("410001","删除角色范围业务规则，不可删除"),

    /**
     * message操作异常
     */
    BUSINESS_BASEDATA_MESSAGE_DB_EXCEPTION_CODE("510001","消息管理操作异常"),

    /**
     * 登录异常出错
     */
    BUSINESS_LOGIN_TENANT_ERROR("600001","租户账号错误"),
    BUSINESS_LOGIN_CODE_OR_PASSWORD_ERROR("600002","工号或者密码错误"),

    BUSINESS_LOGOUT_FAIL_ERROR("600100","登出触发异常");

    /**
     * 错误码
     */
    private String code;
    /**
     * 错误消息
     */
    private String message;

    UserCentreExceptionCode(String code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public String getCode() {
        return this.code;
    }

    @Override
    public String getMessage() {
        return this.message;
    }

    @Override
    public void setCode(String s) {
        this.code=code;
    }

    @Override
    public void setMessage(String s) {
        this.message=s;
    }
}
