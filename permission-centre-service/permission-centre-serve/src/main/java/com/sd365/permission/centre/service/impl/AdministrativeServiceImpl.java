package com.sd365.permission.centre.service.impl;

import com.sd365.common.core.annotation.stuffer.IdGenerator;
import com.sd365.common.core.common.exception.BusinessException;
import com.sd365.common.core.common.exception.code.BizErrorCode;
import com.sd365.common.core.common.service.AbstractBusinessService;
import com.sd365.common.util.BeanUtil;
import com.sd365.permission.centre.dao.mapper.AdministrativeMapper;
import com.sd365.permission.centre.entity.Administrative;
import com.sd365.permission.centre.pojo.dto.AdministrativeDTO;
import com.sd365.permission.centre.pojo.query.AdministrativeQuery;
import com.sd365.permission.centre.pojo.query.IdVersionQuery;
import com.sd365.permission.centre.service.AdministrativeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;
/**
 * @Class AdministrativeServiceImpl
 * @Description abel.zhan 2023-02-13 重构增加AbstracBusinessService 以及处理方法冗余代码和重构异常
 * @Author Administrator
 * @Date 2023-02-13  21:29
 * @version 1.0.0
 */
@Service
public class AdministrativeServiceImpl extends AbstractBusinessService implements AdministrativeService {
    /**
     *  不需要同步数据所以没必要 暂时保留
     */
    @Deprecated
    private static final String TABLE_NAME = "administrative";
    /**
     *  区域操作mapper
     */
    @Autowired
    AdministrativeMapper administrativeMapper;
    /**
     *  id生成器
     */
    @Autowired
    private IdGenerator idGenerator;

    @Override
    public Boolean add(@NotNull @Valid AdministrativeDTO administrativeDTO) {
        // 业务规则检查 例如区域编号不可重复等 如果检查结果违反规则则抛出异常
        Administrative administrative = BeanUtil.copy(administrativeDTO, Administrative.class);
        administrative.setId(idGenerator.snowflakeId());
        administrative.setVersion(1L);
        baseDataStuff4Add(administrative);
        return administrativeMapper.insert(administrative) > 0;
    }

    @Override
    public Boolean remove(@NotNull Long id, @NotNull Long version) {
        // 业务规则检查 例如区域编号不可重复等 如果检查结果违反规则则抛出异常

        Example example = new Example(Administrative.class);
        example.createCriteria().andEqualTo("id", id).andEqualTo("version", version);
        return administrativeMapper.deleteByExample(example) > 0;
    }

    @Override
    public Boolean modify(@NotNull @Valid AdministrativeDTO administrativeDTO) {
        // 业务规则检查 例如区域编号不可重复等 如果检查结果违反规则则抛出异常
        Administrative administrative = BeanUtil.copy(administrativeDTO, Administrative.class);
        baseDataStuff4Updated(administrativeDTO);
        Example example = new Example(Administrative.class);
        example.createCriteria().andEqualTo("id", administrativeDTO.getId()).andEqualTo("version", administrativeDTO.getVersion());
        administrative.setUpdatedTime(new Date());
        return administrativeMapper.updateByExampleSelective(administrative, example) > 0;
    }

    @Override
    public List<Administrative> commonQuery(@NotNull  AdministrativeQuery administrativeQuery) {
            return administrativeMapper.commonQuery(administrativeQuery);
    }

    @Override
    public AdministrativeDTO queryById(@NotNull Long id) {
        return  BeanUtil.copy( administrativeMapper.selectById(id),AdministrativeDTO.class);
    }

    @Override
    public Boolean batchDelete(@NotEmpty List<IdVersionQuery> idVersionQueryList) {
           for (IdVersionQuery idVersionQuery : idVersionQueryList) {
               // 可能因为存在关联触发异常这个异常作为SQL异常将会被全局统一异常捕获
               if (!remove(idVersionQuery.getId(), idVersionQuery.getVersion())){
                   throw new BusinessException(BizErrorCode.DATA_DELETE_NOT_FOUND,new Exception(String.format("id:%l version:%l 没有找到对应记录",idVersionQuery.getId(),idVersionQuery.getVersion())));
               }
           }
        return true;
    }
}
