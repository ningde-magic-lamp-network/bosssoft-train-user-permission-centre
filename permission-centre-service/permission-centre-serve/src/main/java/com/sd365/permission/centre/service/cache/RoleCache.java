/**
 * Copyright (C), 2001-2023, www.bosssof.com.cn
 * FileName: ResourceCache
 * Author: Administrator
 * Date: 2023-04-20 14:28:29
 * Description:
 * <p>
 * History:
 * <author> Administrator
 * <time> 2023-04-20 14:28:29
 * <version> 1.0.0
 * <desc> 版本描述
 */
package com.sd365.permission.centre.service.cache;

import com.alibaba.fastjson.JSON;
import com.sd365.common.core.common.exception.BusinessException;
import com.sd365.common.core.common.exception.code.BizErrorCode;
import com.sd365.permission.centre.dao.mapper.ResourceMapper;
import com.sd365.permission.centre.dao.mapper.RoleMapper;
import com.sd365.permission.centre.dao.mapper.RoleResourceMapper;
import com.sd365.permission.centre.entity.Node;
import com.sd365.permission.centre.entity.Role;
import com.sd365.permission.centre.entity.RoleResource;
import com.sd365.permission.centre.pojo.query.ResourceQuery;
import com.sd365.permission.centre.service.config.Global;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @ClassName: ResourceCache
 * @Description:
 * abel.zhan 2023-05-19 在角色进行 增删改查维护的时候可以通过该类完成缓存的同步操作
 * abel.zhan 2023-05-25 改进cachedRoleResources（）循环插入改为 putAll
 * @Author: Administrator
 * @Date: 2023-04-20 14:28
 **/
@Slf4j
@Component
public class RoleCache implements IRoleCache{

    /**
     * 资源操作mapper
     */
    private ResourceMapper resourceMapper;
    /**
     * 操作缓存的redis
     */
    @Resource(name = "roleResourceRedisTemplate")
    private RedisTemplate<String,Object> redisTemplate;

    /**
     * 用于查询角色和资源关系
     */
    @Autowired
    private RoleResourceMapper roleResourceMapper;
    /**
     * 行记录 ROW_DATA_STATUS 插入就是 1  如果被停用就 0
     */
    private static final byte ROW_DATA_STATUS_ACTIVE=1;
    /**
     * 行记录 ROW_DATA_STATUS 插入就是 1  如果被停用就 0
     */
    private static final byte ROW_DATA_STATUS_INACTIVE=0;


    @Override
    public void cachedRoleResources(@NotNull Role role) {

        /***
         * 1 查询角色对应的 资源id
         * 2 将角色id的和资源id的关系存入redis的 hashmap
         */
        Example example=new Example(RoleResource.class);
        example.createCriteria().andEqualTo("roleId",role.getId());
        List<RoleResource> roleResources=roleResourceMapper.selectByExample(example);
        bindingRoleResource2Redis(role, roleResources);

    }

    private void bindingRoleResource2Redis(@NotNull Role role, List<RoleResource> roleResources) {
        // redis建立 HashMap存储，一个角色一个 Hashmap，map内部存储该role所有用的资源
        HashMap<String,RoleResource> roleResourceHashMap=new HashMap<>();
        roleResources.stream().forEach(roleResource -> {
            roleResourceHashMap.put(String.valueOf(roleResource.getId()),roleResource);
        });
        // 缓存当前角色的所有的资源
        redisTemplate.opsForHash().putAll(Global.HASH_ROLE_ID_KEY +String.valueOf(role.getId()),
                roleResourceHashMap);
    }

    @Override
    public void cacheRoleResource(@NotNull Role role, List<RoleResource> roleResourceList) {
        bindingRoleResource2Redis(role,roleResourceList);
    }

    @Override
    public void cachedMultiRolesResrouces(List<Role> roles) {
        roles.stream().forEach(role -> {
            cachedRoleResources(role);
        });

    }
    @Override
    public void updateCacheRoleResources(@NotNull Role role) {
            cachedRoleResources(role);
    }

    @Override
    public void removeCacheRoleResources(@NotNull Long roleId) {
         redisTemplate.delete(Global.HASH_ROLE_ID_KEY+String.valueOf(roleId));
    }
}
