package com.sd365.permission.centre.service.impl;

import com.sd365.common.core.annotation.mybatis.Pagination;
import com.sd365.common.core.annotation.stuffer.IdGenerator;
import com.sd365.common.core.common.service.AbstractBusinessService;
import com.sd365.common.core.mq.ActionType;
import com.sd365.common.util.BeanUtil;
import com.sd365.permission.centre.dao.mapper.CompanyMapper;
import com.sd365.permission.centre.entity.Company;
import com.sd365.permission.centre.pojo.dto.CompanyDTO;
import com.sd365.permission.centre.pojo.query.CompanyQuery;
import com.sd365.permission.centre.pojo.query.IdVersionQuery;
import com.sd365.permission.centre.service.CompanyService;
import com.sd365.permission.centre.service.util.SendMQMessageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.Date;
import java.util.List;

/**
 * @Class CompanyServiceImpl
 * @Description  abel.zhan 2023-02-20 公司服务类重构
 * @Author Administrator
 * @Date 2023-02-20  22:38
 * @version 1.0.0
 */
//TODO 公司数据同步其他服务需要优化
@Service
public class CompanyServiceImpl extends AbstractBusinessService implements CompanyService {
    @Autowired
    private SendMQMessageUtil sendMQMessageUtil;
    private static final String TABLE_NAME = "company";
    /**
     *  公司mapper
     */
    @Resource
    private CompanyMapper companyMapper;
    /**
     *  id生成器
     */
    @Resource
    private IdGenerator idGenerator;

    @Pagination
    @Override
    public List<Company> commonQuery(CompanyQuery companyQuery) {
            return  companyMapper.commonQuery(companyQuery);
    }
    @Override
    public Boolean add(@Valid CompanyDTO companyDTO) {
        Company company = BeanUtil.copy(companyDTO, Company.class);
        company.setId(idGenerator.snowflakeId());
        company.setVersion(1L);
        baseDataStuff4Add(company);
        if (companyMapper.insertCompany(company)>0) {
            sendMQMessageUtil.SendMessage(ActionType.INSERT, SendMQMessageUtil.EXCHANGE_NAME, TABLE_NAME, company);
            return true;
        }
        return false;
    }

    @Override
    public Boolean remove(Long id, Long version) {
        Example example = new Example(Company.class);
        example.createCriteria().andEqualTo("id", id).andEqualTo("version", version);
        if ( companyMapper.deleteByExample(example) > 0) {
                Company company = new Company();
                company.setId(id);
                company.setVersion(version);
                sendMQMessageUtil.SendMessage(ActionType.DELETE, SendMQMessageUtil.EXCHANGE_NAME, TABLE_NAME, company);
            return true;
        }
            return false;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean removeBatch(List<IdVersionQuery> idVersionQueryList) {
            for (IdVersionQuery idVersionQuery : idVersionQueryList) {
                remove(idVersionQuery.getId(), idVersionQuery.getVersion());
            }
        return true;
    }

    @Override
    public Boolean modify(CompanyDTO companyDTO) {
        Company company = BeanUtil.copy(companyDTO, Company.class);
        baseDataStuff4Updated(company);
        Example example = new Example(Company.class);
        example.createCriteria().andEqualTo("id", companyDTO.getId()).andEqualTo("version", companyDTO.getVersion());
        company.setUpdatedTime(new Date());
        if (companyMapper.updateCompany(company) > 0) {
            sendMQMessageUtil.SendMessage(ActionType.UPDATE, SendMQMessageUtil.EXCHANGE_NAME, TABLE_NAME, company);
            return true;
        }
        return false;
    }

    @Override
    public Boolean batchModify(CompanyDTO companyDTO) {
        companyDTO.setUpdatedTime(new Date());
        Company company = BeanUtil.copy(companyDTO, Company.class);
        baseDataStuff4Updated(company);
        Example example = new Example(Company.class);
        example.createCriteria().andEqualTo("id", companyDTO.getId()).andEqualTo("version", companyDTO.getVersion());
        return companyMapper.updateByExampleSelective(company, example) > 0;
    }

    @Override
    public CompanyDTO queryById(Long id) {
        Company company = companyMapper.selectById(id);
        return company!=null ? BeanUtil.copy(company, CompanyDTO.class): new CompanyDTO();
    }

    @Override
    public CompanyDTO copy(Long id) {
        Company company = companyMapper.selectById(id);
        return company != null ? BeanUtil.copy(company, CompanyDTO.class) : new CompanyDTO();

    }
}
