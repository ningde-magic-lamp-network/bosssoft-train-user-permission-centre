/**
 * Copyright (C), -2025, www.bosssof.com.cn
 * @FileName LoginService.java
 * @Author Administrator
 * @Date 2022-10-12  17:06
 * @Description abel.zhan 重构了部分方法增加了注释
 * History:
 * <author> Administrator
 * <time> 2022-10-12  17:06
 * <version> 1.0.0
 * <desc> abel.zhan 2022-10-12 重构 service 和 congroller
 * <br>具体描述:当前版本的问题在于 service设计的时候功能不够内聚导致成为一个贫血的service
 * <br>而使得LoginController的逻辑过去，因此对该类结果做重构，新增 auth 方法 整合相关调用
 */
package com.sd365.permission.centre.service;
import com.sd365.permission.centre.entity.Role;
import com.sd365.permission.centre.entity.User;
import com.sd365.permission.centre.pojo.dto.LoginUserDTO;
import com.sd365.permission.centre.pojo.vo.ResourceVO;
import com.sd365.permission.centre.pojo.vo.UserVO;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * @interface LoginService
 * @Description 实现登录功能
 * abel.zhan 2023-05-10 增加了 根据 角色id数组获取资源的方法 getResourceVO
 * abel.zhan 2024-10-22 重构迁移LoginUserDTO到 pojo/dto模块
 * @Author JiaoBingjie
 * @Date 2020/12/11 14:58
 * @version 1.0.0
 */
public interface LoginService {
    /**
     * 错误的租户
     */
     int LOGIN_VERIFY_CODE_TENANT_ERR=0;
    /**
     * 认证成功 账号 密码 租户一致
     */
    int LOGIN_VERIFY_CODE_SUCCESS=1;
    /**
     *  账号或者密码错误
     */
    int LOGIN_VERIFY_CODE_CODE_OR_PASSWORD_ERR=2;


    /**
     *  存储是返回一个用户
     * @param:  工号和租户账号
     * @return:  返回用户 如果没找到这返回 null
     */
    public UserVO getUserInfo(String code, String tenantAccount);


    /**
     * 用户登陆成功修改用户状态
     * @param userId,status
     * @return status  1 正常用户 0 注销用户 2 锁定用户 3 在线用户
     */
    Boolean updateUserOnlineStatus(Long userId,byte status);


    /**
     * 新增 auth方法 内部调用 认证和更新用户状态
     * @param: 为了兼容 原来的verification 方法调用而采用相同的参数
     * @return: LoginUserDTO.resultCode=0 和 2 代表失败，1 代表成功 成功情况下包含其他信息
     */
    LoginUserDTO auth(String code, String password, String account);


}
