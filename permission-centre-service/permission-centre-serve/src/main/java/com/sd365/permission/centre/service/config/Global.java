/**
 * Copyright (C), 2001-2023, www.bosssof.com.cn
 * FileName: Global
 * Author: Administrator
 * Date: 2023-05-17 20:27:57
 * Description:
 * <p> abel.zhan 2023/05/06 增加
 * History:
 * <author> Administrator
 * <time> 2023-05-17 20:27:57
 * <version> 1.0.0
 */
package com.sd365.permission.centre.service.config;

/**
 * @ClassName: Global
 * @Description: 全局性的配置信息
 * @Author: Administrator
 * @Date: 2023-05-17 20:27
 **/
public class Global {
    /**
     *  角色id 作为标示hashmap的key
     */
    public static final String HASH_ROLE_ID_KEY="uc:cache:hash:role:id:";
    /**
     *  resource id作为缓存key
     */
    public static final String RESOURCE_ID_KEY="uc:cache:resource:id:";
}
