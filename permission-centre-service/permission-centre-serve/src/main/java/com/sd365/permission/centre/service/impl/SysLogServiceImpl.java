package com.sd365.permission.centre.service.impl;

import com.sd365.common.util.DateUtil;
import com.sd365.permission.centre.dao.mapper.SysLogMapper;
import com.sd365.permission.centre.entity.SysLog;
import com.sd365.permission.centre.entity.User;
import com.sd365.permission.centre.service.SysLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.Date;
import java.util.List;

@Service
public class SysLogServiceImpl implements SysLogService {

    @Autowired
    SysLogMapper sysLogMapper;

    @Override
    public List<SysLog> listByCode(String code) {
        Example example = new Example(SysLog.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("code", code);
        return sysLogMapper.selectByExample(example);
    }

    @Override
    public List<SysLog> listByDate(Date from, Date to) {
        Example example = new Example(SysLog.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andBetween("businessTime", from, to);
        return sysLogMapper.selectByExample(example);
    }

    @Override
    public List<SysLog> listByOneMonth() {
        Example example = new Example(SysLog.class);
        Example.Criteria criteria = example.createCriteria();
        Date pastDate = DateUtil.addDays(new Date(), -30);
        criteria.orGreaterThan("businessTime", pastDate);
        return sysLogMapper.selectByExample(example);
    }

}
