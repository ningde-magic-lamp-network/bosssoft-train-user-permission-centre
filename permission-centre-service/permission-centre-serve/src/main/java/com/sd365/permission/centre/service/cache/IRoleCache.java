/**
 * Copyright (C), 2001-2023, www.bosssof.com.cn
 * FileName: IRoleCache
 * Author: Administrator
 * Date: 2023-04-20 15:56:41
 * Description:
 * <p>
 * History:
 * <author> Administrator
 * <time> 2023-04-20 15:56:41
 * <version> 1.0.0
 * <desc> 版本描述
 */
package com.sd365.permission.centre.service.cache;

import com.sd365.permission.centre.entity.Role;
import com.sd365.permission.centre.entity.RoleResource;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @InterfaceName: IRoleCache
 * @Description: 用于更新角色和资源关系的cache
 * abel.zhan 2023-05-24 增加了 cacheRoleResource 重载支持外部传入 List<RoleResource>
 * @Author: Administrator
 * @Date: 2023-04-20 15:56
 **/
public interface IRoleCache {
    /**
     * 缓存角色资源
     * @param role 角色对象
     */
    void cachedRoleResources(@NotNull Role role);

    /**
     * 缓存角色资源
     * @param role 角色对象
     * @param roleResourceList 角色资源列表
     */
    void cacheRoleResource(@NotNull Role role, List<RoleResource> roleResourceList);
    /**
     * 批量缓存角色和资源关系
     * @param roles 角色列表 内部通过循环设置opsForHash缓存
     */
    void cachedMultiRolesResrouces(List<Role> roles);

    /**
     *  修改的时候将更新角色资源缓存
     */
    void updateCacheRoleResources(@NotNull  Role role);

    /**
     * 删除角色的时候移除缓存中的角色和资源对应关系
     * @param roleId 角色id
     */
    void removeCacheRoleResources(@NotNull Long roleId );


}
