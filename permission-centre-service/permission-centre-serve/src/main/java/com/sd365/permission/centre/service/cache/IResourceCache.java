/**
 * Copyright (C), 2001-2023, www.bosssof.com.cn
 * FileName: IResourceCache
 * Author: Administrator
 * Date: 2023-04-20 14:34:17
 * Description:
 * <p>
 * History:
 * <author> Administrator
 * <time> 2023-04-20 14:34:17
 * <version> 1.0.0
 * <desc> 版本描述
 */
package com.sd365.permission.centre.service.cache;

import com.sd365.permission.centre.entity.Resource;
import com.sd365.permission.centre.pojo.query.ResourceQuery;

/**
 * @interface: IResourceCache
 * @Description: 类的主要功能描述
 * @Author: Administrator
 * @Date: 2023-04-20 14:34
 **/
public interface IResourceCache {
    /**
     *  更改数据库的时候调用此接口
     * @param resource
     */
    void updateCache(Resource resource);

    /**
     *  移除数据的时候调用此接口
     * @param id
     */
    void deleteCache(Long id);

    /**
     *  缓存所有的resource
     * @param resourceQuery
     */
    void cachedAllResources(ResourceQuery resourceQuery);


    /**
     *  增加到缓存
     * @param resource
     */
    void add2Cache(Resource resource);

    /**
     *  从缓存取得对象
     * @param id  资源id
     * @return 资源对象
     */
    Resource getCacheResourceById(Long id);
}
