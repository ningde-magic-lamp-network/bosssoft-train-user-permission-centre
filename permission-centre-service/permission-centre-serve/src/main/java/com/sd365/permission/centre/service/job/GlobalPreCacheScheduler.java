package com.sd365.permission.centre.service.job;
import com.sd365.common.cache.redis.lock.Lock;
import com.sd365.permission.centre.entity.Role;
import com.sd365.permission.centre.pojo.query.ResourceQuery;
import com.sd365.permission.centre.service.ResourceService;
import com.sd365.permission.centre.service.RoleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.IdGenerator;

import java.util.Random;
import java.util.UUID;
/**
 * @Class GlobalPreCacheScheduler
 * @Description
 *  abel.zhan 2023-05-17 重构InitReids 为  RoleResourcePreCacheSchedule
 * 该类主要在凌晨重置role和资源关系缓存，以及 资源缓存，此缓存将为网关提供服务
 * @Author Administrator
 * @Date 2023-05-17  19:50
 * @version 1.0.0
 */
@Slf4j
@Component
public class GlobalPreCacheScheduler {
    /**
     * 分布式锁超时时间
     */
    private static final Integer EXPIRE=300*1000;
    /**
     * 注入common-cache包依赖的分布式锁
     */
    @Autowired
    private Lock distributeLock;
    /**
     * 资源服务 用于加载改租户的全部资源
     */
    @Autowired
    private ResourceService resourceService;

    /**
     * 角色服务用于加载角色的资源并且用于缓存
     */
    @Autowired
    private RoleService roleService;

    /**
     *  凌晨1点30分初始化 每天都触发
     *  秒 分钟 小时 日 月 星期 年（可选）  * 代表任何 ？只能用在日和星期中2选择1
     */
    @Scheduled(cron = "0 30 1 * * *")
    public void loadRoleResource2Cache() {
        // 锁定的key
        String key=UUID.randomUUID().toString();
        try {
            distributeLock.getLock(key,EXPIRE);
            //初始化角色
            resourceService.loadResource2Cache( new ResourceQuery());
            //初始化角色和资源关系
            roleService.loadRoleResource2Cache(new Role());
        } catch (Exception e) {
            log.error("初始化resource 和 role-resource到redis失败，详情",e);
        }finally {
            distributeLock.releaseLock(key);
        }

    }
}
