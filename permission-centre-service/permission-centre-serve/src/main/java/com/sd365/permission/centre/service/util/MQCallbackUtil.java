package com.sd365.permission.centre.service.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

/**
 * @Author:
 * @Description: ack确认
 * 确认模式和回退模式
 * @Date: Created in 17:31 2020/12/9
 * @Modified By:
 */
@Slf4j
//@Component
public class MQCallbackUtil implements RabbitTemplate.ConfirmCallback,RabbitTemplate.ReturnCallback{
    @Override
    public void confirm(CorrelationData correlationData, boolean ack, String cause) {
        if (ack) {
            log.info("消息发送ACK成功");
        } else {
            log.error("消息发送ACK失败，原因{}，回掉id:{}", cause, correlationData);
        }
    }

    @Override
    public void returnedMessage(Message message, int replyCode, String replyText, String exchange, String routingKey) {
        log.info("消息未能投递到目标 queue中.");
    }
}
