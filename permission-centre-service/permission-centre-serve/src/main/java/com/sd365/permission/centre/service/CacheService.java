package com.sd365.permission.centre.service;

import com.sd365.permission.centre.pojo.vo.CacheInfoVO;
import com.sd365.permission.centre.pojo.vo.KeyValueVo;
import org.springframework.cache.annotation.CacheConfig;

import java.util.Map;

/**
 * @Version :1.0
 * @PROJECT_NAME: sd365-permission-centre
 * @PACKAGE_NAME:com.sd365.permission.centre.service
 * @NAME: CacheService
 * @author:xuandian
 * @DATE: 2022/7/11 7:46
 * @description: 缓存监控服务接口类
 */
@CacheConfig(cacheNames = "CacheService")
public interface CacheService {

    /**
     * 缓存监控
     */
    CacheInfoVO getCacheInfo(String key);

    /**
     * 根据key 获取对应的值
     *
     * @param key
     * @return
     */
    KeyValueVo queryByKey(String key);

    /**
     * 删除  key
     * @param key
     */
    Boolean deleteByKey(String key);
}
