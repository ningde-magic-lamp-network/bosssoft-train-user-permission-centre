package com.sd365.permission.centre.service;


import com.sd365.common.core.annotation.mybatis.Pagination;
import com.sd365.common.core.annotation.stuffer.CommonFieldStuffer;
import com.sd365.common.core.annotation.stuffer.MethodTypeEnum;
import com.sd365.permission.centre.entity.Module;
import com.sd365.permission.centre.pojo.dto.ModuleDTO;
import com.sd365.permission.centre.pojo.query.IdVersionQuery;
import com.sd365.permission.centre.pojo.query.ModuleQuery;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import java.util.List;

public interface ModuleService {

    @Pagination
    @CommonFieldStuffer(methodType = MethodTypeEnum.QUERY)
    List<Module> commonQuery(ModuleQuery moduleQuery);

    Boolean add(@RequestBody @Valid ModuleDTO moduleDTO);

    @Transactional
    Boolean remove(Long id, Long version);

    /**
     * 批量删除Module
     *
     * @param idVersionQueryList Module批量删除列表
     * @return
     */
    Boolean removeBatch(List<IdVersionQuery> idVersionQueryList);


    @Transactional
    Boolean modify(ModuleDTO moduleDTO);

    @Transactional
    Boolean batchModify(ModuleDTO moduleDTO);

    ModuleDTO queryById(Long id);

}
