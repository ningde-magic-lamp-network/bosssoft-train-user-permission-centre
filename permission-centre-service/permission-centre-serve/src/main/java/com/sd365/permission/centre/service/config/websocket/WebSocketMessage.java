package com.sd365.permission.centre.service.config.websocket;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * @Description:
 * @Author: WengYu
 * @CreateTime: 2022/06/24 22:01
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class WebSocketMessage {
    /**
     * 客户端消息代码
     */
    private Integer code;
    /**
     * 消息
     */
    private String message;


}
