package com.sd365.permission.centre.service;

import com.sd365.permission.centre.entity.SysLog;
import com.sd365.permission.centre.entity.User;

import java.util.Date;
import java.util.List;

public interface SysLogService {

    /**
     * 通过用户code获取对应用户的日志列表
     * @param code
     * @return
     */
    List<SysLog> listByCode(String code);

    /**
     * 通过时间范围获取日志列表
     * @return
     */
    List<SysLog> listByDate(Date from, Date to);

    /**
     * 获取最近一个月的日志列表
     * @return
     */
    List<SysLog> listByOneMonth();
}
