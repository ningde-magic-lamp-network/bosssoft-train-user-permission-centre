package com.sd365.permission.centre.service.impl;

import com.sd365.common.core.common.exception.BusinessException;
import com.sd365.permission.centre.dao.mapper.ResourceMapper;
import com.sd365.permission.centre.dao.mapper.RoleMapper;
import com.sd365.permission.centre.dao.mapper.TenantMapper;
import com.sd365.permission.centre.dao.mapper.UserMapper;
import com.sd365.permission.centre.entity.Resource;
import com.sd365.permission.centre.entity.Role;
import com.sd365.permission.centre.entity.User;
import com.sd365.permission.centre.pojo.dto.LoginUserDTO;
import com.sd365.permission.centre.pojo.vo.ResourceVO;
import com.sd365.permission.centre.pojo.vo.RoleVO;
import com.sd365.permission.centre.pojo.vo.UserVO;
import com.sd365.permission.centre.service.LoginService;
import com.sd365.permission.centre.service.MenuService;
import com.sd365.permission.centre.service.UserOnlineService;
import com.sd365.permission.centre.service.exception.UserCentreExceptionCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.validation.annotation.Validated;
import javax.validation.constraints.NotBlank;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


/**
 *
 *  用户登录服务类
 * <p>
 * 该类用于处理用户认证的两个动作一个是认证一个角色信息获取
 * </p>
 *
 * <p><strong>功能：</strong></p>
 * <ul>
 *   <li>功能1：登录认证包括得到token</li>
 *   <li>功能2：角色信息获取</li>
 * </ul>
 *
 * <p><strong>主要方法和数据成员：</strong></p>
 * <ul>
 *   <li>{@code verification} - 认证用户是否合法得到认证结果以及相关的token返回前端</li>
 * </ul>
 *
 * <p><strong>注意事项：</strong></p>
 * <ul>
 *   <li>用户的认证和角色信息的获取由于前端的结构设定，拆分为2个方法分别调用，通俗登录成功需要完成两个请求</li>
 * </ul>
 * </p>
 * @author Abel Zhan
 * @date 2024-09-03 14:14
 * @version 1.0.0
 * @since 1.0.0
 */
@Service
@Slf4j
@Validated
public class LoginServiceImpl implements LoginService {

    /**
     * 用于操作user表验证账号密码
     */
    @javax.annotation.Resource
    private UserMapper userMapper;
    /**
     *  查询租户相关信息
     */
    @javax.annotation.Resource
    private TenantMapper tenantMapper;
    /**
     * 查询角色相关信息
     */
    @javax.annotation.Resource
    private RoleMapper roleMapper;
    /**
     * 查询资源相关信息针对resource表
     */
    @javax.annotation.Resource
    private ResourceMapper resourceMapper;
    /**
     * 主要用于构建菜单
     */
    @javax.annotation.Resource
    private MenuService menuService;

    /**
     *  引入做登录状态更新和登录进入插入
     */
    @javax.annotation.Resource
    private UserOnlineService userOnlineService;


    /**
     * 验证登录用户状态：账号(code)和租户(account)不匹配，即 "该账户不存在"，返回0
     *                              账号和租户匹配，但密码错误，返回2
     *                              账号和租户存在数据库且匹配，以及密码正确，则登录成功，返回1
     * abel.zhan 2024-10-22 重构,将返回 int 重构为 LoginUserDTO
     * @param code, password, account
     * @return 返回数据则说明没有抛出异常，数据为前端认证的时候需要的数据
     */
    private LoginUserDTO verification(@NotBlank String code, @NotBlank String password, @NotBlank String account) {
        // 查询数据并且做业务异常检查 如果没有异常则返回正常的数据
        User user=userMapper.selectNameByCodePassword(code,password);
        if(ObjectUtils.isEmpty(user)) {
            throw new BusinessException(UserCentreExceptionCode.BUSINESS_LOGIN_CODE_OR_PASSWORD_ERROR);
        }
        if(ObjectUtils.isEmpty(tenantMapper.selectTenantIdByTenantAccount(account))){
            throw new BusinessException(UserCentreExceptionCode.BUSINESS_LOGIN_TENANT_ERROR);
        }
        return LoginUserDTO.builder()
                .name(user.getName())
                .id(user.getId())
                .tel(user.getTel())
                .orgId(user.getOrgId())
                .companyId(user.getCompanyId())
                .tenantId(user.getTenantId())
                .resultCode(LoginServiceImpl.LOGIN_VERIFY_CODE_SUCCESS)
                .build();

    }
    @Override
    public UserVO getUserInfo(String code, String tenantAccount) {

        // 获取租户
        Long tenantId = tenantMapper.selectTenantIdByTenantAccount(tenantAccount);
        User user= userMapper.selectUserByCodeTenantid(code, tenantId);
        // 获取角色列表
        List<Role> roleList=roleMapper.selectRolesByUserId(user.getId());
        // 获取资源列表
        List<ResourceVO> resourceVOList=menuService.getResourceVO(roleList);

        UserVO userVO=new UserVO();
        // 拷贝了相关的id字段
        BeanUtils.copyProperties(user,userVO);

        //转换了角色列表
        List<RoleVO> roleVOList= roleList.stream().map(role->
               RoleVO.builder().code(role.getCode()).name(role.getName()).build()

        ).collect(Collectors.toList());

        // 遍历角色列表，根据角色id找到对应的资源
        roleVOList.forEach(roleVO -> {
                   List<ResourceVO> resourceVoList2= resourceVOList.stream()
                            .filter(resourceVO ->
                                    resourceVO.getId().equals(roleVO.getId())
                            ).collect(Collectors.toList());
                    roleVO.setResourceVOS(resourceVoList2);
                }
                );

        // 设置前端需要的完整的userVO
        userVO.setRoleList(roleVOList);

        return userVO;
    }


    @Override
    public Boolean updateUserOnlineStatus(Long userId,byte status) {
        if (userId == null){
            return false;
        }
        return userMapper.updateUserOnlineStatus(userId,status) > 0;
    }


    @Override
    public LoginUserDTO auth(String code, String password, String account) {
       return verification(code, password, account);
    }

    /**
     *  通过roleId数组获取资源列表
     * @param roledArrays 角色ids取得所有的资源
     * @return 资源列表
     */
    private List<ResourceVO> getResourceVO(Long[] roledArrays) {
        // 批量查询取得角色所包含的所有的资源
        List<Resource> resources=resourceMapper.getResourceByRoleId(Arrays.asList(roledArrays));
        // 这个service按规范应该是dto出去的 这个代码有待优化 另外这个功能也很奇怪
        return menuService.buildMenu(resources);
    }
}
