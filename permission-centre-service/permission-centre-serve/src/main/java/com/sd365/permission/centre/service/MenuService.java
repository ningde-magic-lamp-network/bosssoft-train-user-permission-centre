package com.sd365.permission.centre.service;

import com.sd365.permission.centre.entity.Resource;
import com.sd365.permission.centre.entity.Role;
import com.sd365.permission.centre.pojo.vo.ResourceVO;

import java.util.List;

/**
 * @author No_one
 * @description 用于根据资源生成菜单，菜单即资源的层级关系，ResourceVO中包含List<ResourceVO>，代表子结点
 */
public interface MenuService {
    /**
     * 标记本节点就是父亲节点
     */
    int IS_PARENT=-1;

    /**
     * 根据角色的资源生成菜单
     * @param resourceIdList
     * @return 构建父亲节点的列表
     */
    List<ResourceVO> buildMenu(List<Resource> resourceIdList);

    /**
     * 根据角色获对应的菜单列表，这个菜单是没有体现层级的
     * @param roles 角色列表
     * @return 资源列表
     */
    List<ResourceVO> getResourceVO(List<Role> roles);
}
