package com.sd365.permission.centre.api.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.sd365.permission.centre.entity.Role;
import com.sd365.permission.centre.service.RoleService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collection;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
@EnableCaching
@Slf4j
public class RoleServiceTest {
    @Autowired
    private RedisTemplate redisTemplate;
    /**
     * 测试角色
     */
    @Autowired
    private RoleService roleService;
    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void loadRoleResource2Cache() {
        roleService.loadRoleResource2Cache(new Role());
    }

    @Test
    void getRoleResourceFromCache(){

        Map userMap= redisTemplate.opsForHash().entries("uc:cache:hash:role:id:1337645864428109824");
        Collection collection= userMap.values();
        log.info(JSONObject.toJSONString(collection));
    }
    @Test
    void testUpdateRole(){

    }

}