package com.sd365.permission.centre.api.service.impl;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.sd365.permission.centre.entity.Resource;
import com.sd365.permission.centre.pojo.dto.ResourceDTO;
import com.sd365.permission.centre.pojo.query.ResourceQuery;
import com.sd365.permission.centre.service.ResourceService;
import com.sd365.permission.centre.service.config.Global;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.IdGenerator;

import javax.validation.constraints.AssertTrue;
import java.util.Date;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
@EnableCaching
@Slf4j
class ResourceServiceImplTest {
    /**
     * 验证redis对象使用,一定要指定使用某一个 redisTemplate
     */
    @javax.annotation.Resource(name="roleResourceRedisTemplate")
    private RedisTemplate<String,Object> redisTemplate;
    /**
     * 被测试对象
     */
    @Autowired
    private ResourceService resourceService;
    private Long id = new Long(1);

   // private ResourceQuery resourceQuery= BeanUtil.copy(resourceService.queryById(id),ResourceQuery.class);

    @Test
    Boolean add() {
        ResourceDTO resourceDTO = new ResourceDTO();
        resourceDTO.setCreatedBy(111L);
        resourceService.add(resourceDTO);
        return true;
    }

    @Test
    void batchDelete() {
    }

    @Test
    void modify() {
        ResourceDTO resource = new ResourceDTO();
        resource.setId(new Long(1));
        resource.setVersion(new Long(1));
        resource.setUpdatedTime(new Date());
        resourceService.modify(resource);
    }

    @Test
    void copy() {
    }

    @Test
    void testloadResource2Cache(){
       List<ResourceDTO> resourceDTOList= resourceService.loadResource2Cache(new ResourceQuery());
       resourceDTOList.stream().forEach(resourceDTO -> {
            Object resource= redisTemplate.opsForValue().get(Global.RESOURCE_ID_KEY+resourceDTO.getId());
            Assert.assertEquals(JSON.parseObject(JSONObject.toJSONString(resource),Resource.class) .getName(),resourceDTO.getName());
        });
    }


    @Test
    void testUpdateResource(){
        ResourceDTO resourceDTO= resourceService.queryById(1205448843721632676L);
        resourceDTO.setRemark("测试修改20230519v1");
        resourceService.modify(resourceDTO);
        ResourceDTO resourceDTO2= resourceService.queryById(1205448843721632676L);
        Assert.assertTrue(resourceDTO2.getRemark().trim().equals("测试修改20230519v1"));
    }
    @Test
    void testAddResource(){
        ResourceDTO resourceDTO= resourceService.queryById(1205448843721632676L);
        resourceDTO.setId(100L);
        resourceDTO.setCode("T100");
        resourceDTO.setRemark("测试增加20230519v1");
        resourceService.add(resourceDTO);
        ResourceDTO resourceDTO2= resourceService.queryById(100L);
        Assert.assertTrue(resourceDTO2.getRemark().trim().equals("测试增加20230519v1"));
    }

    @Test
    void testDeleteResource(){

        ResourceDTO resourceDTO2= resourceService.queryById(100L);
        Assert.assertTrue(resourceService.remove(resourceDTO2.getId()));

    }


}