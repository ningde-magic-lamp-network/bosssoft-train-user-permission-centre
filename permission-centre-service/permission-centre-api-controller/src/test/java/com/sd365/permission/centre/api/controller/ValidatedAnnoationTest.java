/**
 * Copyright (C), 2001-2023, www.bosssof.com.cn
 * FileName: ValidatedAnnoationTest
 * Author: Administrator
 * Date: 2023-03-13 17:20:41
 * Description:
 * <p>
 * History:
 * <author> Administrator
 * <time> 2023-03-13 17:20:41
 * <version> 1.0.0
 * <desc> 版本描述
 */
package com.sd365.permission.centre.api.controller;

import com.sd365.permission.centre.entity.User;
import com.sd365.permission.centre.pojo.dto.UserDTO;
import org.junit.Test;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * @ClassName: ValidatedAnnoationTest
 * @Description: 类的主要功能描述
 * @Author: Administrator
 * @Date: 2023-03-13 17:20
 **/
public class ValidatedAnnoationTest {

    private ClassUseForTest classUseForTest=new ClassUseForTest();
    @Test
    public void notnullTest(){
        classUseForTest.testNotNull(null);
        UserDTO userDTO=new UserDTO();
        classUseForTest.testObjectAttributeNotNull(userDTO);

    }
    @Validated
    public class ClassUseForTest{

        public void testNotNull(@NotNull Long id){
            System.out.println("right");
        }

        public void testObjectAttributeNotNull(@Valid UserDTO userDTO){
            System.out.println("testObjectAttributeNotNull right");
        }
    }
}
