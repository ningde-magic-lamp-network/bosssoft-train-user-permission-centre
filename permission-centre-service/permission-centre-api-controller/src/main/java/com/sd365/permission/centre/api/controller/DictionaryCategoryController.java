package com.sd365.permission.centre.api.controller;

import com.sd365.common.core.common.api.CommonResponse;
import com.sd365.common.core.common.api.CommonResponseUtils;
import com.sd365.common.log.api.annotation.ApiLog;
import com.sd365.common.util.BeanUtil;
import com.sd365.permission.centre.api.DictionaryCategoryApi;
import com.sd365.permission.centre.entity.DictionaryType;
import com.sd365.permission.centre.pojo.dto.DictionaryTypeDTO;
import com.sd365.permission.centre.pojo.query.DictionaryCategoryQuery;
import com.sd365.permission.centre.pojo.vo.DictionaryCategoryVO;
import com.sd365.permission.centre.service.DictionaryCategoryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;
import java.util.List;


/**
 * <p>对字典类型 api的实现</p>
 * @author 周靖赟
 * @version 1.0
 * @since  12/15/20
 */
@Slf4j
@RestController
public class DictionaryCategoryController implements DictionaryCategoryApi {
    @Autowired
    private DictionaryCategoryService dictionaryCategoryService;

    @ApiLog
    @Override
    public List<DictionaryCategoryVO> commonQuery(@NotNull DictionaryCategoryQuery query) {
        return dictionaryCategoryService.commonQuery(query);
    }

    @ApiLog
    @Override
    public DictionaryCategoryVO info(@PathVariable("id") Long id){
        return dictionaryCategoryService.info(id);
    }

    @ApiLog
    @Override
    public Boolean save(@RequestBody DictionaryTypeDTO dictionaryCategory) {
        return dictionaryCategoryService.add(BeanUtil.copy(dictionaryCategory, DictionaryType.class));
    }

    @ApiLog
    @Override
    public CommonResponse<Boolean> update(@RequestBody DictionaryTypeDTO dictionaryCategory) {
		Integer result = dictionaryCategoryService.modify(BeanUtil.copy(dictionaryCategory, DictionaryType.class));
		if (result == -1) {
		    return CommonResponseUtils.failed("已有同名类型");
        } else {
		    return CommonResponseUtils.success(result == 1);
        }
    }

    @ApiLog
    @Override
    public CommonResponse<Boolean> delete(DictionaryTypeDTO dictionaryCategory) {
        Boolean result = null;
        try {
            result = dictionaryCategoryService.delete(BeanUtil.copy(dictionaryCategory, DictionaryType.class));
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        if (result == null) {
            return CommonResponseUtils.failed("该类型被其他表使用无法删除");
        } else {
            return CommonResponseUtils.success(result);
        }
    }

    @ApiLog
    @Override
    public CommonResponse<Boolean> batchDelete(List<DictionaryTypeDTO> dictionaryCategoryList) {
        try {
            return CommonResponseUtils.success(dictionaryCategoryService.batchDelete(BeanUtil.copyList(dictionaryCategoryList, DictionaryType.class)));
        } catch (Exception e) {
            return CommonResponseUtils.failed("一些类型被其他表使用无法删除");
        }
    }

}
