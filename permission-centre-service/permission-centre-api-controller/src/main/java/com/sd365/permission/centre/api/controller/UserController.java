/**
 * Copyright (C), 2022-2025, www.bosssof.com.cn
 * @FileName UserController.java
 * @Author Administrator
 * @Date 2022-9-28  21:35
 * @Description 该文件为具体的用户管理界面的对应的控制器类
 * History:
 * <author> Administrator
 * <time> 2022-9-28  21:35
 * <version> 1.0.0
 * <desc> 该文件为具体的用户管理界面的对应的控制器类
 */
package com.sd365.permission.centre.api.controller;

import com.sd365.common.log.api.annotation.ApiLog;
import com.sd365.common.util.BeanUtil;
import com.sd365.permission.centre.api.UserApi;
import com.sd365.permission.centre.entity.Department;
import com.sd365.permission.centre.entity.Position;
import com.sd365.permission.centre.entity.Role;
import com.sd365.permission.centre.entity.User;
import com.sd365.permission.centre.pojo.dto.UserCentreDTO;
import com.sd365.permission.centre.pojo.dto.UserDTO;
import com.sd365.permission.centre.pojo.query.UserQuery;
import com.sd365.permission.centre.pojo.vo.ResourceVO;
import com.sd365.permission.centre.pojo.vo.UserVO;
import com.sd365.permission.centre.service.MenuService;
import com.sd365.permission.centre.service.UserService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 用户管理模块
 * <p>
 * 该类用于处理用户管理的增删改查等功能，其中重点的是包含了 为用户分配角色的功能，
 * </p>
 *
 * <p><strong>功能：</strong></p>
 * <ul>
 *   <li>功能1：普通的增删改查</li>
 *   <li>功能2： 用户相关的资源获取</li>
 *   <li>功能3：为用户分配角色</li>
 * </ul>
 *
 * <p><strong>主要方法和数据成员：</strong></p>
 * <ul>
 *   <li>{@code getUserResourceVO} - 使用 {@code getUserResourceVO} 获取用户所拥有的菜单信息其中已经构建好父子关系。</li>
 *   <li>{@code getRoleResourceVO} - 使用 {@code getRoleResourceVO} 获取角色所拥有的菜单信息其中已经构建好父子关系。</li>
 *   <li>{@code modifyUserInfoForBm} - 使用 {@code modifyUserInfoForBm} 其他平台调用该接口修改用户信息。</li>
 *   <li>{@code modifyWithNewRole} - 使用 {@code modifyWithNewRole} 为用户分配角色。</li>
 *    <li>{@code queryAllRole} - 使用 {@code queryAllRole} 查询用户所有的角色显示在界面。</li>
 *     <li>{@code queryAllDepartment} - 使用 {@code queryAllDepartment} 查询用户所有的部门。</li>
 * </ul>
 *
 * <p><strong>注意事项：</strong></p>
 * <ul>
 *  <li>暂无</li>
 * </ul>
 * </p>
 * @author Abel Zhan
 * @date 2024-09-03 14:14
 * @version 1.0.0
 * @since 1.0.0
 */
@Validated
@RestController
public class UserController implements UserApi {

    /**
     *  用户服务
     */
    @Resource
    private UserService userSerice;
    /**
     *  菜单资源的服务，例如获取角色的菜单，菜单父子关系构建等
     */
    @Resource
    private MenuService menuService;

    @Override
    public List<ResourceVO> getUserResourceVO(Long userId) {
        return userSerice.getUserRoleResourceVO(userId);
    }

    @Override
    public List<ResourceVO> getRoleResourceVO(@NotEmpty Long[] roleIdArray) {
        ;
        return menuService.getResourceVO(Arrays.stream(roleIdArray).map(roleId -> {
            Role role= new Role();
            role.setId(roleId);
            return role;
        }).collect(Collectors.toList()));
    }

    @ApiLog
    @Override
    public Boolean add(@RequestBody  @NotNull  @Valid UserDTO userDTO) {
        return userSerice.add(userDTO);
    }

    @ApiLog
    @Override
    public Boolean remove(Long id, Long version) {
        return userSerice.remove(id, version);
    }

    @ApiLog
    @Override
    public Boolean modify(@Valid UserDTO userDTO) {
        return userSerice.modify(userDTO);
    }

    @Override
    public Boolean modifyUserInfo(UserCentreDTO userCentreDTO) {
        return userSerice.modifyUserInfo(userCentreDTO);
    }

    @Override
    public Boolean modifyUserInfoForBm(UserCentreDTO userCentreDTO) {

        return userSerice.modifyUserInfoForBm(userCentreDTO);
    }

    @Override
    public Boolean modifyWithNewRole( UserDTO[] userDTOS) {
        return userSerice.modifyWithNewRole(userDTOS);
    }

    @ApiLog
    @Override
    public List<User> commonQuery(UserQuery userQuery) {
        return userSerice.commonQuery(userQuery);
    }


    @Override
    public UserVO queryUserById(@NotNull  Long id) {
        User user = userSerice.queryById(id);
       return user!=null ?  BeanUtil.copy(user, UserVO.class): new UserVO();

    }

    @Override
    public List<Role> queryAllRole() {
        return userSerice.queryAllRole();
    }

    @Override
    public List<Department> queryAllDepartment() {
        return userSerice.queryAllDepartment();
    }

    @Override
    public List<Position> queryAllPosition() {
        return userSerice.queryAllPosition();
    }

    @Override
    public Boolean batchUpdate(@Valid UserDTO[] userDTOS) {
        return userSerice.batchUpdate(userDTOS);
    }

    @Override
    public Boolean batchDelete(@Valid UserDTO[] userDTOS) {
        return userSerice.batchDelete(userDTOS);
    }

    @Override
    public Boolean firstStartMd5() {
        return true;
    }

    @Override
    public Boolean updatePassword(@Valid UserDTO userDTO) {
        return userSerice.updatePassword(userDTO);
    }

    @Override
    public User getUser(String code) {
        return userSerice.getUser(code);
    }
}
