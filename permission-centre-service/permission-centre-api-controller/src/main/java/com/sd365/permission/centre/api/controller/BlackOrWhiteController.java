package com.sd365.permission.centre.api.controller;

import com.sd365.common.core.annotation.mybatis.Pagination;
import com.sd365.common.log.api.annotation.ApiLog;
import com.sd365.common.util.BeanUtil;
import com.sd365.permission.centre.api.BlackOrWhiteApi;
import com.sd365.permission.centre.pojo.dto.BlackOrWhiteDTO;
import com.sd365.permission.centre.pojo.query.BlackOrWhiteQuery;
import com.sd365.permission.centre.pojo.vo.BlackOrWhiteVO;
import com.sd365.permission.centre.service.BlackOrWhiteService;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;
/**
 * @Class BlackOrWhiteController
 * @Description 提供黑白名单查询接口
 * @Author Administrator
 * @Date 2023-02-20  20:57
 * @version 1.0.0
 */
@RestController
public class BlackOrWhiteController implements BlackOrWhiteApi{
    /**
     * 黑白名单服务类
     */
    @Resource
    private BlackOrWhiteService blackOrWhiteService;
    @ApiLog
    @Override
    public List<BlackOrWhiteVO> commonQuery(BlackOrWhiteQuery blackOrWhiteQuery) {
        //执行查询
        return  blackOrWhiteService.commonQuery(blackOrWhiteQuery);
    }
    @ApiLog
    @Override
    public Boolean batchUpdate(List<BlackOrWhiteDTO> blackOrWhiteDTOS) {
        // @TODO 还没有实现
        return false;
    }

    @ApiLog
    @Override
    public Boolean add(BlackOrWhiteDTO blackOrWhiteDTO) {
        return ObjectUtils.isEmpty(blackOrWhiteService.add(blackOrWhiteDTO));
    }

    @ApiLog
    @Override
    public Boolean remove(Long id, Long version) {
        return blackOrWhiteService.remove(id,version);
    }

    @ApiLog
    @Override
    public Boolean batchDelete(@Valid BlackOrWhiteDTO[] blackOrWhiteDTOS) {
        return blackOrWhiteService.batchRemove(blackOrWhiteDTOS);
    }

    @ApiLog
    @Override
    public Boolean modify(@Valid BlackOrWhiteDTO blackOrWhiteDTO) {
        return blackOrWhiteService.modify(blackOrWhiteDTO);
    }

    @ApiLog
    @Override
    public BlackOrWhiteVO queryById(Long id) {
        BlackOrWhiteDTO blackOrWhiteDTO = blackOrWhiteService.queryById(id);
        return blackOrWhiteDTO!=null ? BeanUtil.copy(blackOrWhiteDTO,BlackOrWhiteVO.class) : new BlackOrWhiteVO();
    }

    @ApiLog
    @Override
    public Boolean batchAdd(List<BlackOrWhiteDTO> blackOrWhiteDTOList){
        return blackOrWhiteService.batchAdd((blackOrWhiteDTOList));
    }

    @ApiLog
    @Override
    public BlackOrWhiteVO testRedis(BlackOrWhiteQuery blackOrWhiteQuery){
        return blackOrWhiteService.queryTest(blackOrWhiteQuery);
    }
}
