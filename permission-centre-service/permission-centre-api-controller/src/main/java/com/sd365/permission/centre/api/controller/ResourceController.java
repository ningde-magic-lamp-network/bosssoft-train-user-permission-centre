/**
 * Copyright (C), 2001-${YEAR}, www.bosssof.com.cn
 * @FileName ResourceController.java
 * @Author Administrator
 * @Date 2022-9-28  11:29
 * @Description 角色管理模块，该模块维护系统的菜单、按钮 以及 不可显示的URL资源
 * History:
 * <author> Administrator
 * <time> 2022-9-28  11:29
 * <version> 1.0.0
 * <desc> 2023年2月25重构
 */
package com.sd365.permission.centre.api.controller;

import com.sd365.common.log.api.annotation.ApiLog;
import com.sd365.common.util.BeanUtil;
import com.sd365.permission.centre.api.ResourceApi;
import com.sd365.permission.centre.pojo.dto.ResourceDTO;
import com.sd365.permission.centre.pojo.query.ResourceQuery;
import com.sd365.permission.centre.pojo.vo.ResourceVO;
import com.sd365.permission.centre.pojo.vo.SubSystemVO;
import com.sd365.permission.centre.service.ResourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@RestController
/**
 * @Class ResourceController
 * @Description 资源管理类实现对资源的增删改查
 * @Author Administrator
 * @Date 2022-9-28  11:29
 * @version 1.0.0
 */
public class ResourceController implements ResourceApi {
    /**
     *  资源管理类
     */
    @Autowired
    private ResourceService resourceService;

    @Override
    public List<ResourceVO> initRedis(@NotNull  ResourceQuery resourceQuery) {
        return  BeanUtil.copyList(resourceService.loadResource2Cache(resourceQuery), ResourceVO.class);
    }
    @ApiLog
    @Override
    public List<ResourceVO> commonQuery(@NotNull ResourceQuery resourceQuery) {
        List<ResourceDTO> resourceDTOS = resourceService.commonQuery(resourceQuery);
        return resourceDTOS!= null
            ?  BeanUtil.copyList(resourceDTOS, ResourceVO.class, new BeanUtil.CopyCallback() {
                @Override
                public void copy(Object o, Object o1) {
                    ResourceDTO resourceDTO = (ResourceDTO) o;
                    ResourceVO resourceVO = (ResourceVO) o1;

                    if(resourceDTO.getResourceDTO()!=null){
                        ResourceVO copy = BeanUtil.copy(resourceDTO.getResourceDTO(), ResourceVO.class);
                        resourceVO.setResourceVO(copy);
                    }
                    if(resourceDTO.getSubSystem()!=null){
                        BeanUtil.copy(resourceDTO.getSubSystem(), resourceVO.getSubSystemVO(), SubSystemVO.class);
                    }
                }
            }): new ArrayList<>();
    }

    @ApiLog
    @Override
    public List<ResourceVO> queryResourceTree(@NotNull ResourceQuery resourceQuery) {
        return  BeanUtil.copyList(resourceService.query(resourceQuery), ResourceVO.class);
    }
    @ApiLog
    @Override
    public Boolean add(@NotNull @Valid @RequestBody ResourceDTO resourceDTO) {
        return  ObjectUtils.isEmpty(resourceService.add(resourceDTO));
    }

    @Override
    public ResourceVO queryResourceById(@NotNull Long id) {
        ResourceDTO resourceDTO= resourceService.queryById(id);
        return   resourceDTO!=null ? BeanUtil.copy(resourceService.queryById(id), ResourceVO.class)
                : new ResourceVO();
    }

    @Override
    public ResourceVO queryResourceByParentId(Long id) {
        return BeanUtil.copy(resourceService.findByParentId(id), ResourceVO.class);
    }

    @Override
    public List<ResourceVO> queryResourceByMenu() {
        return  BeanUtil.copyList(resourceService.queryResourceByMenu(), ResourceVO.class);
    }

    @Override
    public Boolean remove(@NotNull Long id) {
        return resourceService.remove(id);
    }

    @Override
    public Boolean batchDelete(@NotNull @Valid @RequestBody ResourceDTO[] resourceDTOS) {
        return resourceService.batchDelete(resourceDTOS);
    }

    @Override
    public Boolean modify(@NotNull @Valid @RequestBody ResourceDTO resourceDTO) {
        return ObjectUtils.isEmpty(resourceService.modify(resourceDTO));
    }
    @Override
    public ResourceDTO copy(@NotNull Long id) {
        return resourceService.copy(id);
    }
}
