package com.sd365.permission.centre.api.controller;

import com.sd365.common.core.common.exception.BusinessException;
import com.sd365.common.core.common.exception.code.CommonErrorCode;
import com.sd365.common.log.api.annotation.ApiLog;
import com.sd365.common.util.BeanUtil;
import com.sd365.permission.centre.api.AuditApi;
import com.sd365.permission.centre.entity.Audit;
import com.sd365.permission.centre.pojo.dto.AuditDTO;
import com.sd365.permission.centre.pojo.query.AuditQuery;
import com.sd365.permission.centre.pojo.vo.AuditVO;
import com.sd365.permission.centre.service.AuditService;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
public class AuditController implements AuditApi {
    /**
     * 审核服务
     */
    @Resource
    private AuditService auditService;
    /**
     * 通用查询，带分页信息
     * @param auditQuery
     * @return
     */
    @ApiLog
    @Override
    public List<AuditVO> commonQuery(AuditQuery auditQuery) {
        return  BeanUtil.copyList(auditService.commonQuery(auditQuery), AuditVO.class);
    }

    @Override
    @ApiLog
    public AuditVO selectById(Long id) {
        return BeanUtil.copy(auditService.queryById(id),AuditVO.class);
    }

    @Override
    public List<AuditVO> selectByUseSimplePwd(Byte isusp) {
        return BeanUtil.copyList(auditService.selectByUseSimplePwd(isusp),AuditVO.class);
    }

    @Override
    public List<AuditVO> selectByNoUpdatePwd(Byte isnup) {
        return  BeanUtil.copyList(auditService.selectByNoUpdatePwd(isnup),AuditVO.class);
    }

    @Override
    public List<AuditVO> selectByLongNoLogin(Byte islnl) {
        return  BeanUtil.copyList(auditService.selectByLongNoLogin(islnl),AuditVO.class);
    }
}
