package com.sd365.permission.centre.api.controller;

import com.sd365.common.core.common.controller.AbstractController;
import com.sd365.common.core.common.exception.BusinessException;
import com.sd365.common.core.common.exception.code.CommonErrorCode;
import com.sd365.common.log.api.annotation.ApiLog;
import com.sd365.common.util.BeanUtil;
import com.sd365.permission.centre.api.SystemParameterApi;
import com.sd365.permission.centre.pojo.dto.DeleteSystemParamDTO;
import com.sd365.permission.centre.pojo.dto.SystemParamDTO;
import com.sd365.permission.centre.pojo.query.SystemParameterFuzzyQuery;
import com.sd365.permission.centre.pojo.query.SystemParameterQuery;
import com.sd365.permission.centre.pojo.vo.SystemParamVO;
import com.sd365.permission.centre.service.SystemParameterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
public class SystemParameterController extends AbstractController implements SystemParameterApi {

    @Autowired
    private SystemParameterService systemParameterService;
    @ApiLog
    @Override
    public Boolean add(@Valid SystemParamDTO systemParamDTO) {
        return systemParameterService.add(systemParamDTO);
    }
    @ApiLog
    @Override
    public Boolean remove(Long id, Long version) {
        return systemParameterService.remove(id,version);
    }

    @Override
    public Boolean batchRemove(@Valid DeleteSystemParamDTO[] deleteSystemParamDTOS) {
        return systemParameterService.batchDelete(deleteSystemParamDTOS);
    }

    @Override
    public Boolean batchUpdate(@Valid SystemParamDTO[] systemParamDTOS) {
        return systemParameterService.batchUpdate(systemParamDTOS);
    }

    @ApiLog
    @Override
    public Boolean modify(@Valid SystemParamDTO systemParameDTO) {
        return systemParameterService.modify(systemParameDTO);
    }
    @ApiLog
    @Override
    public List<SystemParamVO> commonQuery(SystemParameterQuery systemParameterQuery) {
        if(null==systemParameterQuery){
            throw new BusinessException(CommonErrorCode.SYSTEM_CONTROLLER_ARGUMENT_NOT_VALID,new Exception("查询对象不允许为空"));
        }
        //执行查询
        List<SystemParamDTO> systemParamDTOS= systemParameterService.commonQuery(systemParameterQuery);
        List<SystemParamVO> systemParamVOS= BeanUtil.copyList(systemParamDTOS,SystemParamVO.class);
        return systemParamVOS;
    }

    @ApiLog
    @Override
    public List<SystemParamVO> fuzzyQuery(SystemParameterFuzzyQuery systemParameterFuzzyQuery) {
        if(null==systemParameterFuzzyQuery){
            throw new BusinessException(CommonErrorCode.SYSTEM_CONTROLLER_ARGUMENT_NOT_VALID,new Exception("查询对象不允许为空"));
        }
        //执行查询
        List<SystemParamDTO> systemParamDTOS= systemParameterService.fuzzyQuery(systemParameterFuzzyQuery);
        List<SystemParamVO> systemParamVOS= BeanUtil.copyList(systemParamDTOS,SystemParamVO.class);
        return systemParamVOS;
    }

    @Override
    public SystemParamVO querySystemParameterById(Long id) {
        SystemParamDTO systemParamDTO=systemParameterService.queryById(id);
        if(systemParamDTO!=null){
            SystemParamVO systemParamVO=BeanUtil.copy(systemParamDTO, SystemParamVO.class);
            return systemParamVO;
        }else{
            return null;
        }
    }

    @Override
    public SystemParamVO QueryByName(String name) {
        SystemParamDTO systemParamDTO=systemParameterService.queryByName(name);
        if(systemParamDTO!=null){
            SystemParamVO systemParamVO=BeanUtil.copy(systemParamDTO,SystemParamVO.class);
            return systemParamVO;
        }else{
            return null;
        }
    }
}
