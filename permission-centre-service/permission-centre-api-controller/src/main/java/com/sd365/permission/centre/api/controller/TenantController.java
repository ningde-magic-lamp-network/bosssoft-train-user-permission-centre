package com.sd365.permission.centre.api.controller;

import com.sd365.common.core.common.controller.AbstractController;
import com.sd365.common.core.common.exception.BusinessException;
import com.sd365.common.core.common.exception.code.CommonErrorCode;
import com.sd365.common.log.api.annotation.ApiLog;
import com.sd365.common.util.BeanUtil;
import com.sd365.permission.centre.api.TenantApi;
import com.sd365.permission.centre.pojo.dto.DeleteTenantDTO;
import com.sd365.permission.centre.pojo.dto.TenantDTO;
import com.sd365.permission.centre.pojo.query.TenantQuery;
import com.sd365.permission.centre.pojo.vo.TenantVO;
import com.sd365.permission.centre.service.TenantService;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
/**
 * @Class TenantController
 * @Description 租户实现类，接口的功能请参考接口注释
 * @Author Administrator
 * @Date 2023-03-15  8:24
 * @version 1.0.0
 */
@RestController
public class TenantController implements TenantApi {
    /**
     *  租户服务
     */
    @Autowired
    private TenantService tenantService;

    @Override
    public Boolean add(@NotNull  @Valid  @RequestBody TenantDTO tenantDTO) {
        return tenantService.add(tenantDTO);
    }

    @ApiLog
    @Override
    public List<TenantVO> commonQuery(@NotNull TenantQuery tenantQuery) {
        return BeanUtil.copyList(tenantService.commonQuery(tenantQuery), TenantVO.class);

    }

    @Override
    public Boolean remove(@ApiParam(value = "当前行id", required=true) @NotNull @RequestParam("id")  Long id,
                          @ApiParam(value="当前行版本",required = true)@NotNull @RequestParam("version") Long version) {
        return tenantService.remove(id,version);
    }

    @Override
    public Boolean modify(@NotNull @Valid @RequestBody TenantDTO tenantDTO) {
        return tenantService.modify(tenantDTO);
    }

    @Override
    public Boolean batchRemove(@NotNull @Valid @RequestBody  DeleteTenantDTO[] deleteTenantDTOS) {
        return tenantService.batchDelete(deleteTenantDTOS);
    }

    @Override
    public TenantVO queryTenantById(@NotNull @PathVariable Long id) {
        //throw new BusinessException(CommonErrorCode.SYSTEM_SERVICE_ARGUMENT_NOT_VALID,new Exception("测试异常"));
        TenantDTO tenantDTO=tenantService.queryById(id);
        return tenantDTO!=null  ? BeanUtil.copy(tenantDTO,TenantVO.class) :new TenantVO() ;

    }

    @Override
    public TenantVO queryByName(String name) {
        TenantDTO tenantDTO=tenantService.queryByName(name);
        return tenantDTO!=null ? BeanUtil.copy(tenantDTO,TenantVO.class) : new TenantVO();
    }
}
