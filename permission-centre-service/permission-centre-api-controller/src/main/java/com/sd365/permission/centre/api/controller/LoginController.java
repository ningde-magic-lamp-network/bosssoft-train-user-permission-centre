package com.sd365.permission.centre.api.controller;

import cn.hutool.http.HttpStatus;
import com.alibaba.fastjson.JSON;
import com.sd365.common.core.context.BaseContextHolder;
import com.sd365.permission.centre.api.LoginApi;
import com.sd365.permission.centre.entity.Role;
import com.sd365.permission.centre.entity.User;
import com.sd365.permission.centre.pojo.dto.CompanyDTO;
import com.sd365.permission.centre.pojo.dto.LoginUserDTO;
import com.sd365.permission.centre.pojo.vo.ResourceVO;
import com.sd365.permission.centre.pojo.vo.UserVO;
import com.sd365.permission.centre.service.CompanyService;
import com.sd365.permission.centre.service.LoginService;
import com.sd365.permission.centre.service.UserOnlineService;
import com.sd365.permission.centre.service.util.Md5Utils;
import lombok.extern.slf4j.Slf4j;
import lombok.extern.slf4j.XSlf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.LinkedList;
import java.util.List;

/**
 *  登录业务处理类
 *
 * <p>
 *  改用用于处理 登录界面调用的 认证和角色资源获取的接口以及用户登出注销，
 *  前端通过 认证接口获取基本的用户信息和token
 *  前端通过 角色资源获取接口获取角色资源，
 * </p>
 *
 * <p><strong>功能：</strong></p>
 * <ul>
 *   <li>功能1：认证</li>
 *   <li>功能2：角色资源获取</li>
 *   <li>功能3：注销</li>
 * </ul>
 *
 * <p><strong>主要方法和数据成员：</strong></p>
 * <ul>
 *   <li>{@code auth} - 完成登录的验证</li>
 *   <li>{@code info} - 根据账号和租户获取角色列表和对应的资源</li>
 *   <li>{@code logout} - 注销</li>
 *
 * </ul>
 *
 * <p><strong>注意事项：</strong></p>
 * <ul>
 *   <li>该类应通过依赖注入从容器中获取，不能直接使用 {@code new} 关键字实例化。</li>
 * </ul>
 * </p>
 * @author Abel Zhan
 * @date 2024-09-03 14:14
 * @version 1.0.0
 * @since 1.0.0
 */
@RestController
@Slf4j
@Validated
public class LoginController  implements LoginApi {

    /**
     * 登录服务
     */
    @Resource
    private LoginService loginService;
    /**
     * 公司服务
     */
    @Resource
    private CompanyService companyService;
    /**
     * 用户在线服务
     */
    @Resource
    private UserOnlineService userOnlineService;
    /**
     * redis缓存的token key
     */
    private static final String USER_TOKEN_KEY = "user:token:";
    /**
     * redis缓存的i用户登录信息 key
     */
    private static final String USER_LOGING_KEY = "user:loginInfo:";

    /**
     * 其中就是login方法 ，
     * @param code  登录界面的工号
     * @param password 密码
     * @param tenantAccount 租户账号
     * @return 登录的用户信息
     */
    @Override
    public LoginUserDTO auth(@NotBlank @RequestParam(value = "code") String code, @NotBlank  @RequestParam(value = "password") String password,
                             @NotBlank  @RequestParam(value = "tenantAccount") String tenantAccount) {
        // 根据user.code, user.password, tenant.account
        return loginService.auth(code, password, tenantAccount);
    }
    @Deprecated
    @Override
    public LoginUserDTO login(@NotBlank @RequestParam(value = "code") String code,
                              @NotBlank @RequestParam(value = "password") String password,
                              @NotBlank @RequestParam(value = "tenantAccount") String tenantAccount) {
        // 根据user.code, user.password, tenant.account
        return loginService.auth(code, password, tenantAccount);
    }

    @Override
    public UserVO info(@NotBlank @RequestParam(value = "code") String code,
                       @NotBlank @RequestParam(value = "account") String tenantAccount) {
        return  loginService.getUserInfo(code, tenantAccount);

    }
    @Override
    public String logout() {
        String logoutId = BaseContextHolder.getUserIdStr();
        userOnlineService.forceLogout(logoutId);
        return logoutId;
    }
}
