package com.sd365.permission.centre.api.controller;

import com.sd365.common.core.common.controller.AbstractController;
import com.sd365.common.log.api.annotation.ApiLog;
import com.sd365.common.util.BeanUtil;
import com.sd365.permission.centre.api.SysUpgradeLogApi;
import com.sd365.permission.centre.pojo.dto.SysUpgradeLogDTO;
import com.sd365.permission.centre.pojo.query.IdVersionQuery;
import com.sd365.permission.centre.pojo.query.SysUpgradeLogQuery;
import com.sd365.permission.centre.pojo.vo.SubSystemVO;
import com.sd365.permission.centre.pojo.vo.SysUpgradeLogVO;
import com.sd365.permission.centre.pojo.vo.TenantVO;
import com.sd365.permission.centre.service.SysUpgradeLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

/**
 * @className: SysUpgradeLogController
 * @description: 升级日志Controller
 * @author: lxr
 * @date: 2022/07/14
 */

@RestController
public class SysUpgradeLogController extends AbstractController implements SysUpgradeLogApi {

    @Autowired
    SysUpgradeLogService sysUpgradeLogService;


    @ApiLog
    @Override
    public List<SysUpgradeLogVO> commonSysUpgradeLogQuery(SysUpgradeLogQuery sysUpgradeLogQuery) {
        List<SysUpgradeLogDTO> sysUpgradeLogDTOS = sysUpgradeLogService.commonQuerySysUpgradeLog(sysUpgradeLogQuery);
        List<SysUpgradeLogVO> sysUpgradeLogVOS = BeanUtil.copyList(sysUpgradeLogDTOS, SysUpgradeLogVO.class, new BeanUtil.CopyCallback() {
            @Override
            public void copy(Object o, Object o1) {
                SysUpgradeLogDTO sysUpgradeLogDTO = (SysUpgradeLogDTO) o;
                SysUpgradeLogVO sysUpgradeLogVO = (SysUpgradeLogVO) o1;
                if(sysUpgradeLogDTO.getTenantDTO() != null){
                    TenantVO copy = BeanUtil.copy(sysUpgradeLogDTO.getTenantDTO(),  TenantVO.class);
                    sysUpgradeLogVO.setTenantVO(copy);
                }
                if(sysUpgradeLogDTO.getSubSystemDTO() != null){
                    SubSystemVO copy = BeanUtil.copy(sysUpgradeLogDTO.getSubSystemDTO(), SubSystemVO.class);
                    sysUpgradeLogVO.setSubSystemVO(copy);
                }
            }
        });
        return sysUpgradeLogVOS;
    }

    @Override
    public SysUpgradeLogVO latestOne(Long tenantId, Long subSystemId) {
        SysUpgradeLogDTO sysUpgradeLogDTO = sysUpgradeLogService.latestOne(tenantId, subSystemId);
        if (sysUpgradeLogDTO == null) {
            return null;
        }
        SysUpgradeLogVO sysUpgradeLogVO = BeanUtil.copy(sysUpgradeLogDTO, SysUpgradeLogVO.class);
        if (sysUpgradeLogDTO.getSubSystemDTO() != null) {
            SubSystemVO subSystemVO = BeanUtil.copy(sysUpgradeLogDTO.getSubSystemDTO(), SubSystemVO.class);
            sysUpgradeLogVO.setSubSystemVO(subSystemVO);
        }
        if (sysUpgradeLogDTO.getTenantDTO() != null) {
            TenantVO tenantVO = BeanUtil.copy(sysUpgradeLogDTO.getTenantDTO(), TenantVO.class);
            sysUpgradeLogVO.setTenantVO(tenantVO);
        }
        return sysUpgradeLogVO;
    }

    @Override
    public SysUpgradeLogVO queryById(Long id) {
        SysUpgradeLogDTO sysUpgradeLogDTO = sysUpgradeLogService.queryById(id);
        if (sysUpgradeLogDTO != null) {
            SysUpgradeLogVO sysUpgradeLogVO = BeanUtil.copy(sysUpgradeLogDTO, SysUpgradeLogVO.class);
            return sysUpgradeLogVO;
        } else {
            return null;
        }
    }

    @Override
    public Boolean add(@Valid SysUpgradeLogDTO sysUpgradeLogDTO) {
        return sysUpgradeLogService.add(sysUpgradeLogDTO);
    }

    @Override
    public Boolean deleteOneById(Long id, Long version) {
        return sysUpgradeLogService.remove(id, version);
    }

    @Override
    public Boolean deleteBatch(List<IdVersionQuery> batchQuery) {
        return sysUpgradeLogService.deleteBatch(batchQuery);
    }
}
