package com.sd365.permission.centre.api.controller;

import com.sd365.common.log.api.annotation.ApiLog;
import com.sd365.common.util.BeanUtil;
import com.sd365.permission.centre.api.DictionaryApi;
import com.sd365.permission.centre.pojo.dto.DictionaryDTO;
import com.sd365.permission.centre.pojo.query.DictionaryQuery;
import com.sd365.permission.centre.pojo.vo.DictionaryVO;
import com.sd365.permission.centre.service.DictionaryService;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

/**
 * @class DictionaryController
 * @classdesc
 * @author Administrator
 * @date 2020-10-8  9:58
 * @version 1.0.0
 * @see
 * @since
 */
@RestController
public class DictionaryController  implements DictionaryApi {
    /**
     * 客户服务类
     */
    @Autowired
    private DictionaryService dictionaryService;
    /**
     *  该组件同步数据到其他系统
     */
    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Override
    public Boolean add( @NotNull @Valid @RequestBody DictionaryDTO DictionaryDTO) {
        return dictionaryService.add(DictionaryDTO);
    }

    @Override
    public Boolean remove(@NotNull @RequestParam("id")  Long id, @NotNull @RequestParam("version")  Long version) {
        return dictionaryService.remove(id,version);
    }

    @Override
    public Boolean modify(@NotNull @Valid @RequestBody DictionaryDTO dictionaryDTO) {
        return dictionaryService.modify(dictionaryDTO);
    }

    @ApiLog
    @Override
    public Boolean batchUpdate(@NotNull @RequestBody DictionaryDTO[] DictionaryDTOS) {
        return dictionaryService.batchUpdate(DictionaryDTOS);
    }

    @ApiLog
    @Override
    public Boolean batchRemove(@NotNull @RequestBody DictionaryDTO[] DictionaryDTOS) {
        return dictionaryService.batchRemove(DictionaryDTOS);
    }

    @Override
    public DictionaryDTO copy( @NotNull Long id) {
        return null;
    }


    @ApiLog
    @Override
    public List<DictionaryVO> commonQuery(@NotNull DictionaryQuery dictionaryQuery) {
        //执行查询
        return BeanUtil.copyList(dictionaryService.commonQuery(dictionaryQuery),DictionaryVO.class);

    }

    @ApiLog
    @Override
    public DictionaryVO queryDictionaryById( @NotNull  @PathVariable("id")  Long id) {
        DictionaryDTO dictionaryDTO=dictionaryService.queryById(id);
        return dictionaryDTO!=null ? BeanUtil.copy(dictionaryDTO,DictionaryVO.class) : new DictionaryVO();
    }

    @Override
    public List<DictionaryVO> queryDictionaryByTypeId( @NotNull @PathVariable("id") Long id) {
        List<DictionaryDTO> dictionaryDTOS=dictionaryService.queryByTypeId(id);
        return dictionaryDTOS!=null ? BeanUtil.copyList(dictionaryDTOS,DictionaryVO.class):new ArrayList<>();
    }
}
