package com.sd365.permission.centre.api.controller;

import com.sd365.common.core.common.exception.BusinessException;
import com.sd365.common.core.common.exception.code.CommonErrorCode;
import com.sd365.common.log.api.annotation.ApiLog;
import com.sd365.common.util.BeanUtil;
import com.sd365.permission.centre.api.CompanyApi;
import com.sd365.permission.centre.entity.Company;
import com.sd365.permission.centre.pojo.dto.CompanyDTO;
import com.sd365.permission.centre.pojo.query.CompanyQuery;
import com.sd365.permission.centre.pojo.query.IdVersionQuery;
import com.sd365.permission.centre.pojo.vo.CompanyVO;
import com.sd365.permission.centre.service.CompanyService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
/**
 * @Author jxd
 * @Date 2020/12/12  1:05 下午
 * @Version 1.0
 * @Write For CompanyController
 * @Email waniiu@126.com
 */
@RestController
public class CompanyController implements CompanyApi {
    @Resource
    private CompanyService companyService;
    @Override
    @ApiLog
    public List<CompanyVO> commonQuery(CompanyQuery companyQuery) {
            return  BeanUtil.copyList(companyService.commonQuery(companyQuery), CompanyVO.class);
    }

    @Override
    public Boolean add(@Valid CompanyDTO companyDTO) {
        return companyService.add(companyDTO);
    }

    @Override
    public Boolean remove(Long id, Long version) {
        return companyService.remove(id, version);
    }
    @Override
    @ApiLog
    public Boolean removeBatch(List<IdVersionQuery> idVersionQueryList) {
        return companyService.removeBatch(idVersionQueryList);
    }

    @Override
    public Boolean modify(@Valid CompanyDTO companyDTO) {
        return companyService.modify(companyDTO);
    }

    @ApiLog
    @Override
    public CompanyVO queryCompanyById(@Valid @NotNull Long id) {
        CompanyDTO companyDTO = companyService.queryById(id);
        return companyDTO!=null ? BeanUtil.copy(companyDTO, CompanyVO.class) : new CompanyVO();

    }

    @Override
    public CompanyDTO copy(Long id) {
        return companyService.copy(id);
    }

    @Override
    @ApiLog
    public Boolean batchUpdate(List<CompanyDTO> companyDTOS) {
        System.out.println("batchUpdate");
        Boolean flag = true;
        // 批量删除
        for (int i = 0; i < companyDTOS.size(); ++i) {
            CompanyDTO dto = companyDTOS.get(i);
            flag = companyService.batchModify(dto);
            if (flag == false) {
                return false;
            }
        }
        return flag;
    }

    @Override
    @ApiLog
    public Boolean status(@RequestBody CompanyDTO companyDTO) {
        return companyService.batchModify(companyDTO);
    }
}
