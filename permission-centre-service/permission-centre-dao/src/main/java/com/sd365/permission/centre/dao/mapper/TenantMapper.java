package com.sd365.permission.centre.dao.mapper;

import com.sd365.common.core.common.dao.CommonMapper;
import com.sd365.permission.centre.entity.Tenant;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface TenantMapper extends CommonMapper<Tenant> {
    List<Tenant> commonQuery(Tenant tenant);

    Tenant selectById(Long id);

    Tenant queryByName(String name);

    int updateTenant(Tenant tenant);


    /**
     * @description 根据租户账号查找租户id，方便后续关联查询用户账户
     * @param account
     * @return
     */
    Long selectTenantIdByTenantAccount(String account);

}