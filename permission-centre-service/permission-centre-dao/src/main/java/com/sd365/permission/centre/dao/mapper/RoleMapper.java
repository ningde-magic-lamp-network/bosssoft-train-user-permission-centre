package com.sd365.permission.centre.dao.mapper;

import com.sd365.common.core.common.dao.CommonMapper;
import com.sd365.permission.centre.entity.Node;
import com.sd365.permission.centre.entity.Role;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface RoleMapper extends CommonMapper<Role> {

    List<Role> selectRoleByUserid (Long userId);

    /**
     * 每个Mapper都可以定义属于自己通用查询
     * @param role 查询对象一般是entity
     * @return 查询到的列表
     */
    List<Role> commonQuery(Role role);

    /**
     * 根据id 使用多表链接查询查找role对象
     * @param id role 的 id
     * @return 带有 公司 机构  客户等字段的 role 对象
     */
    Role selectById(Long id);

    /**
     * 查询表中的公司
     * @param
     * @return 带有 公司 机构  客户等字段的 role 对象
     */
    List<Node> selectCompany();

    /**
     * 判断用户名是否已经存在
     * @param role
     * @return
     */
    int haveRole(Role role);

    List<Role> selectRolesByUserId(Long userId);

}