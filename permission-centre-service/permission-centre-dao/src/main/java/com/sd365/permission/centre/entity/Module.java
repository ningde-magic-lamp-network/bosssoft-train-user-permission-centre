package com.sd365.permission.centre.entity;

import com.sd365.common.core.common.pojo.entity.BaseEntity;
import com.sd365.common.core.common.pojo.entity.TenantBaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Table;

/**
 * @author 王海堂
 * @date 2022/07/081402
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "com.sd365.permission.centre.entity.Module")
@Table(name = "basic_module")
public class Module extends BaseEntity {
    /**
     * 模块名字
     */
    @ApiModelProperty(value = "name模块名字")
    private String name;
    /**
     * 模块编码
     */
    @ApiModelProperty(value = "code模块编码")
    private String code;
    /**
     * 模块描述
     */
    @ApiModelProperty(value = "description模块描述")
    private String description;
    /**
     * 版本
     */
    @ApiModelProperty(value = "edition版本")
    private String edition;
    /**
     * 子系统id
     */
    @ApiModelProperty(value = "subsystem_id子系统id")
    private Long subsystem_id;

    @ApiModelProperty(value = "companyId公司id")
    private Long companyId;

    @ApiModelProperty(value = "modifier修改者")
    private String modifier;

    @ApiModelProperty(value = "creator创建者")
    private String creator;

    @ApiModelProperty(value = "orgId组织")
    private Long orgId;

    @ApiModelProperty(value = "tenantId租户")
    private Long tenantId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEdition() {
        return edition;
    }

    public void setEdition(String edition) {
        this.edition = edition;
    }

    public Long getSubsystem_id() {
        return subsystem_id;
    }

    public void setSubsystem_id(Long subsystem_id) {
        this.subsystem_id = subsystem_id;
    }

    public String getModifier() {
        return modifier;
    }

    public void setModifier(String modifier) {
        this.modifier = modifier;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public Long getTenantId() {
        return tenantId;
    }

    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }
}
