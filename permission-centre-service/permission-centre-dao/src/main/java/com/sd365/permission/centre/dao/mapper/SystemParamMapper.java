package com.sd365.permission.centre.dao.mapper;

import com.sd365.common.core.common.dao.CommonMapper;
import com.sd365.permission.centre.entity.SystemParam;
import com.sd365.permission.centre.pojo.dto.SystemParamDTO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface SystemParamMapper extends CommonMapper<SystemParam> {
    /**
     *  根据id 使用多表链接查询查找SystemParameter对象
     * @param id  SystemParameter 的 id
     * @return 带有 公司 机构  结算类型等字段的SystemParameter 对象
     */
    SystemParam selectById(Long id);

    /**
     * 每个Mapper都可以定义属于自己通用查询
     * @param systemParameter 查询对象一般是entity
     * @return 查询到的列表
     */
    List<SystemParam> commonQuery(SystemParam systemParameter);

    /**
     * 模糊查询
     * @param systemParamDTO
     * @return 查询到的列表
     */
    List<SystemParam> fuzzyQuery(SystemParamDTO systemParamDTO);

    /**
     * 每个Mapper都可以定义属于自己通过列表删除
     * @param systemParameterList 查询对象一般List
     * @return 成功则为true，删除列表选中元素
     */
    SystemParam queryByName(String name);

}