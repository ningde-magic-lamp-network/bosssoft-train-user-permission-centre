package com.sd365.permission.centre.dao.mapper;

import com.sd365.common.core.common.dao.CommonMapper;
import com.sd365.permission.centre.entity.UserRole;

import java.util.List;

/**
 * @Interface UserRoleMapper
 * @Description
 * 建议使用Example更简单，除非多表关联的才需要自定义 xml
 * @Author Administrator
 * @Date 2023-06-08  16:18
 * @version 1.0.0
 */
public interface UserRoleMapper extends CommonMapper<UserRole> {
    List<UserRole> selectByUserId(Long id);
    /**
     * 删除用户的所有的角色
     * @param id  用户id
     * @return  影响的行数
     */
    Integer deleteAllByUserID(Long id);

    /**
     *  判断角色记录是否存在
     * @param userRole
     * @return 返回记录数
     */
    int haveUserRole(UserRole userRole);

    /**
     *  查询租户对应的用户id ，同一个租户下的用户不允许相同的code，但是id肯定是不通的
     * @param userId 用户id
     * @param tenantId 租户id
     * @return 用户和角色关系
     */
    UserRole selectUserRoleByUseridTenantid(Long userId, Long tenantId);

}