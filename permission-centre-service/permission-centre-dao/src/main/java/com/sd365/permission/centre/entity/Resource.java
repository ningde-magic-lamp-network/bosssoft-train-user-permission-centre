package com.sd365.permission.centre.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.sd365.common.core.common.pojo.entity.TenantBaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.Date;

@ApiModel(value="com.sd365.permission.centre.entity.Resource")
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Table(name = "basic_resource")
public class Resource extends TenantBaseEntity implements Serializable {
    @Transient
    private Resource resource;

    /**
     * 资源所属的系统
     */
    @Transient
    private SubSystem subSystem;
    /**
     * 资源名称  注意这个也可以是界面上的按钮
     */
    @ApiModelProperty(value="name资源名称  注意这个也可以是界面上的按钮")
    private String name;

    /**
     * 子系统ID，预留
     */
    @ApiModelProperty(value="subSystemId子系统ID，预留")
    private Long subSystemId;

    /**
     * 编号
     */
    @ApiModelProperty(value="code编号 ")
    private String code;

    /**
     * 顺序号，控制菜单显示顺序
     */
    @ApiModelProperty(value="orderIndex顺序号，控制菜单显示顺序")
    private Integer orderIndex;

    /**
     * 父亲节点
     */
    @ApiModelProperty(value="parentId父亲节点")
    private Long parentId;

    /**
     * URL 这个决定加载什么组件 对应import组件
     */
    @ApiModelProperty(value="urlURL 这个决定加载什么组件 对应import组件")
    private String url;

    /**
     * 后端接口地址读音组件的 path属性
     */
    @ApiModelProperty(value="api后端接口地址读音组件的 path属性")
    private String api;

    /**
     * 请求方法，和 api 搭配使用, 1 GET, 2 POST, 3 PUT,4  DELETE
     */
    @ApiModelProperty(value="method请求方法，和 api 搭配使用, 1 GET, 2 POST, 3 PUT,4  DELETE")
    private Byte method;

    /**
     * 打开图标 这个只能取系统预定义的
     */
    @ApiModelProperty(value="openImg打开图标 这个只能取系统预定义的")
    private String openImg;

    /**
     * 关闭图标  这个只能取系统预定义的
     */
    @ApiModelProperty(value="closeImg关闭图标  这个只能取系统预定义的")
    private String closeImg;

    /**
     * 资源类型  这个是菜单还是按钮 0 菜单 1 按钮
     */
    @ApiModelProperty(value="resourceType资源类型  这个是菜单还是按钮 0 菜单 1 按钮  ")
    private Byte resourceType;

    /**
     * 叶子节点
     */
    @ApiModelProperty(value="leaf叶子节点")
    private Byte leaf;

    /**
     * 动作 这个暂时保留
     */
    @ApiModelProperty(value="action动作 这个暂时保留")
    private Byte action;

    /**
     * 备注
     */
    @ApiModelProperty(value="remark备注")
    private String remark;

    /**
     * 创建人
     */
    @ApiModelProperty(value="创建人")
    private Long createdBy;


    @ApiModelProperty(value="createdTime")
    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    private Date createdTime;

    /**
     *
     */
    @ApiModelProperty(value="updatedTime")
    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    private Date updatedTime;

    /**
     * 公司
     */
    @ApiModelProperty(value="company")
    private Company company;

    /**
     * 机构
     */
    @ApiModelProperty(value="organization")
    private Organization organization;


    @ApiModelProperty(value="tenant")
    private Tenant tenant;



}
