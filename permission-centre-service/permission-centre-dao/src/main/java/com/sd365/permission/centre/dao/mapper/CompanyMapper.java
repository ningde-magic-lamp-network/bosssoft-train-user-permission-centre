package com.sd365.permission.centre.dao.mapper;

import com.sd365.common.core.common.dao.CommonMapper;
import com.sd365.permission.centre.entity.Company;
import com.sd365.permission.centre.pojo.query.CompanyQuery;

import java.util.List;

public interface CompanyMapper extends CommonMapper<Company> {
    /**
     * 通用查询带分页
     * @param companyQuery
     * @return
     */
    List<Company> commonQuery(CompanyQuery companyQuery);

    int insertCompany(Company company);

    int updateCompany(Company company);

    Company selectById(Long id);

    /**
     * 获取当前机构下的所有公司
     * @param orgId
     * @return
     */
    List<Company> findAllByOrgId(Long orgId);
}