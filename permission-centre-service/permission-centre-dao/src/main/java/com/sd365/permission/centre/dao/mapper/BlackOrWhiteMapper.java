package com.sd365.permission.centre.dao.mapper;

import com.sd365.common.core.common.dao.CommonMapper;
import com.sd365.permission.centre.entity.BlackOrWhite;
import com.sd365.permission.centre.pojo.query.BlackOrWhiteQuery;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface BlackOrWhiteMapper extends CommonMapper<BlackOrWhite> {
    List<BlackOrWhite> commonQuery(BlackOrWhiteQuery blackOrWhiteQuery);
}