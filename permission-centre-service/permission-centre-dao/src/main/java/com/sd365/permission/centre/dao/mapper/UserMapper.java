package com.sd365.permission.centre.dao.mapper;

import com.sd365.common.core.common.dao.CommonMapper;
import com.sd365.permission.centre.entity.Role;
import com.sd365.permission.centre.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
public interface UserMapper extends CommonMapper<User> {
    /**
     * 根据名字查询是否已有数据，目的为防止重复添加
     */
    User selectByCode(String code);

    User selectById(Long id);

    /**
     * 修改user，不修改中间表，也就是不修改角色
     *
     * @param user
     * @return
     */
    Integer update(@Param("user") User user);

    Integer updateStatus(@Param("user") User user);

    List<Role> selectRoleByUserid(Long userId);

    List<User> commonQuery(User user);

    List<User> commonByQueryRoleId(@Param("user") User user, @Param("roleId") Long roleId);

    User selectUserByCodeTenantid(String code, Long tenantId);

    User selectNameByCodePassword(String code,String password);

    /**
     * 用户登陆后修改状态不修改版本号
     * @param userId
     * @return
     */
    Integer updateUserOnlineStatus(@Param("userId") Long userId,@Param("status") byte status);

    List<User> longNoLoginUserList();
}