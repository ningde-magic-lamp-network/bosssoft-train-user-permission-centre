package com.sd365.permission.centre.entity;

import com.sd365.common.core.common.pojo.entity.TenantBaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Table;
import javax.persistence.Transient;

@ApiModel(value="com.sd365.permission.centre.entity.Dictionary")
@Table(name = "basic_dictionary")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Dictionary extends TenantBaseEntity {
    @Transient
    private Organization organization;
    @Transient
    private Company company;
    /**
     * 职位名称
     */
    @ApiModelProperty(value="name职位名称")
    private String name;

    @Transient
    private DictionaryType dictionaryType;

    /**
     * 字典类型ID
     */
    @ApiModelProperty(value="dictionaryTypeId字典类型ID")
    private Long dictionaryTypeId;

    /**
     * 参数值
     */
    @ApiModelProperty(value="value参数值")
    private String value;

    /**
     * 备注
     */
    @ApiModelProperty(value="remark备注")
    private String remark;

    /**
     * 职位名称
     */
    public String getName() {
        return name;
    }

    /**
     * 职位名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 字典类型ID
     */

    /**
     * 字典类型ID
     */

    /**
     * 参数值
     */
    public String getValue() {
        return value;
    }

    /**
     * 参数值
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * 备注
     */
    public String getRemark() {
        return remark;
    }

    /**
     * 备注
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Long getDictionaryTypeId() {
        return dictionaryTypeId;
    }

    public void setDictionaryTypeId(Long dictionaryTypeId) {
        this.dictionaryTypeId = dictionaryTypeId;
    }
}
