package com.sd365.permission.centre.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.sd365.common.core.common.pojo.entity.TenantBaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Table;
import java.util.Date;
import java.util.Objects;

@Data
@ApiModel(value="com.sd365.permission.centre.entity.RoleResource")
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "basic_role_resource")
public class RoleResource extends TenantBaseEntity {
    /**
     * 角色ID
     */
    @ApiModelProperty(value="roleId角色ID")
    private Long roleId;

    /**
     * 资源ID
     */
    @ApiModelProperty(value="resourceId资源ID")
    private Long resourceId;

    /**
     * 预计到达时间
     */
    @ApiModelProperty(value="predictReachTime")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date createdTime;

    /**
     * 修改时间
     */
    @ApiModelProperty(value="updateTime")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date updatedTime;

    /**
     * 公司
     */
    @ApiModelProperty(value="company")
    private Company company;

    /**
     * 机构
     */
    @ApiModelProperty(value="organization")
    private Organization organization;

    /**
     * 租户
     */

    @ApiModelProperty(value="tenant")
    private Tenant tenant;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        RoleResource that = (RoleResource) o;
        return Objects.equals(roleId, that.roleId) &&
                Objects.equals(resourceId, that.resourceId) &&
                Objects.equals(tenant, that.tenant);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), roleId, resourceId, tenant);
    }
}