package com.sd365.permission.centre.api;

import com.sd365.common.core.annotation.mybatis.Pagination;
import com.sd365.common.log.api.annotation.ApiLog;
import com.sd365.permission.centre.pojo.dto.DeleteTenantDTO;
import com.sd365.permission.centre.pojo.dto.TenantDTO;
import com.sd365.permission.centre.pojo.query.TenantQuery;
import com.sd365.permission.centre.pojo.vo.TenantVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
/**
 * @Class TenantApi
 * @Description 租户管理，租户数据的创立有几个渠道
 * 1、门户申请创建租户平台审核通过
 * 2、后台管理员增加租户
 * 特别说明：创建租户后系统自动创建该租户所对应的管理员并且授予租户的角色资源。
 * 平台将配置若干种资源用于租户选择相应的角色，这些角色通过其他数据维护手段内置在系统中，
 * 增加租户的时候只要相应选择即可
 * @Author Administrator
 * @Date 2023-03-15  8:19
 * @version 1.0.0
 */
@CrossOrigin
@Validated
@Api(tags = "租户管理",value = "/permission/centre/v1/tenant")
@RequestMapping(value = "/permission/centre/v1/tenant")
public interface TenantApi {

    /**
     * 增加租户 门户用户注册租户触发或者后台管理员增加租户
     * @param tenantDTO
     * @return 成功则true CommonResponse 应答码和消息统一参考基础框架
     */
    @ApiOperation(value="增加租户")
    @PostMapping(value="")
    @ResponseBody
    Boolean add(@NotNull  @Valid  @RequestBody TenantDTO tenantDTO);

    /**
     * 查询租户 支持多条件查询
     * @param tenantQuery
     * @return 租户列表
     */
    @ApiOperation(value="查询租户")
    @GetMapping(value = "")
    @ResponseBody
    List<TenantVO> commonQuery(@NotNull TenantQuery tenantQuery);

    /**
     *  删除租户
     * @param id  当前行的记录
     * @param version 当前行的版本
     * @return true成功 false失败
     */
    @ApiOperation(value="删除租户")
    @DeleteMapping(value="")
    @ResponseBody
    Boolean remove(@ApiParam(value = "当前行id", required=true) @NotNull @RequestParam("id") Long id,
                   @ApiParam(value="当前行版本",required = true)@NotNull @RequestParam("version") Long version);

    /**
     * 门户或者后台修改租户
     * @param tenantDTO
     * @return
     */
    @ApiOperation(value="修改租户")
    @PutMapping(value = "")
    @ResponseBody
    Boolean modify( @NotNull @Valid @RequestBody TenantDTO tenantDTO);

    /**
     * 批量删除租户
     * @param deleteTenantDTOS  租户列表 因为要带版本和id所以使用对象数组
     * @return true成功false失败
     */
    @ApiOperation(value="批量删除租户")
    @DeleteMapping(value="/batch")
    @ResponseBody
    Boolean batchRemove(@NotNull @Valid @RequestBody DeleteTenantDTO[] deleteTenantDTOS);

    /**
     * 通过id 查询租户
     * @param id 租户记录id
     * @return 租户对象
     */

    @ApiOperation(value="查询租户通过ID")
    @GetMapping(value = "/{id}")
    @ResponseBody
    TenantVO queryTenantById(@NotNull @PathVariable("id") Long id);

    /**
     *  查询是否存在同名租户用于判断租户
     * @param name  租户名字
     * @return 租户数据
     */
    @ApiOperation(value = "查询是否存在租户名")
    @GetMapping(value = "/queryByName")
    @ResponseBody
    TenantVO queryByName(String name);
}
