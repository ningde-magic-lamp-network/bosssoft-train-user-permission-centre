package com.sd365.permission.centre.api;

import com.sd365.permission.centre.entity.Tenant;
import com.sd365.permission.centre.pojo.dto.SubSystemDTO;
import com.sd365.permission.centre.pojo.query.SubSystemQuery;
import com.sd365.permission.centre.pojo.vo.SubSystemVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin
@Api(tags = "子系统管理 ",value ="/permission/centre/subsystem")
@RequestMapping(value = "/permission/centre/subsystem")
public interface SubSystemApi {

    /**
     * @param: 子系统DTO
     * @return: 成功则true CommonResponse 应答码和消息统一参考基础框架
     * @see: 添加
     * @since
     */
    @PostMapping(value="")
    @ResponseBody
    Boolean add(@RequestBody @Valid SubSystemDTO subSystemDTO);


    /**
     * @param: 当前行id和当前行版本
     * @return: 成功则true
     * @see： 删除
     * @since
     */
    @DeleteMapping(value="")
    @ResponseBody
    Boolean remove(@ApiParam(value = "当前行id", required = true) @RequestParam("id") Long id,
                   @ApiParam(value = "当前行版本", required = true) @RequestParam("version") Long version);


    /**
     * @param: 子系统DTO数组
     * @return: 成功则true
     * @see： 批量删除
     * @since
     */
    @DeleteMapping(value="/batch")
    @ResponseBody
    Boolean batchDelete(@RequestBody @Valid SubSystemDTO[] subSystemDTOS);

    /**
     * @param: 子系统DTO
     * @return: 成功则true
     * @see： 修改
     * @since
     */
    @PutMapping(value = "")
    @ResponseBody
    Boolean modify(@Valid SubSystemDTO subSystemDTO);

    /**
     * @param: 模糊查询条件
     * @return: 查询的DTOS
     * @see： 模糊查询
     * @since
     */
    @GetMapping(value = "")
    @ResponseBody
    List<SubSystemVO> commonQuery(SubSystemQuery subSystemQuery);

    /**
     * @param: id
     * @return: 查询的DTO
     * @see： 查询子系统通过ID
     * @since
     */
    @GetMapping(value = "{id}")
    @ResponseBody
    SubSystemVO querySubSystemById(@PathVariable("id") Long id);

    /**
     * @param: id
     * @return: 查询的DTO
     * @see： 查询子系统通过tenantId
     * @since
     */
    @GetMapping(value = "/tenantId")
    @ResponseBody
    List<SubSystemVO> querySubSystemByTenantId(@RequestParam("tenantId") @Valid Long tenantId);

    /**
     * @param: 查询租户
     * @return: 查询的DTOS
     * @see： 模糊查询
     * @since
     */
    @GetMapping(value = "/tenant")
    @ResponseBody
    List<Tenant> tenantQuery();
}
