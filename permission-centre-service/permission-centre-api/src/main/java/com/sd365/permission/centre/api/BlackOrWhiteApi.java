package com.sd365.permission.centre.api;

import com.sd365.permission.centre.pojo.dto.BlackOrWhiteDTO;
import com.sd365.permission.centre.pojo.query.BlackOrWhiteQuery;
import com.sd365.permission.centre.pojo.vo.BlackOrWhiteVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;
/**
 * @Class BlackOrWhiteApi
 * @Description 黑白名单维护界面的管理类，主要是针对UI的管理功能
 * @Author Administrator
 * @Date 2023-02-20  21:08
 * @version 1.0.0
 */
@CrossOrigin
@Api(tags = "黑白名单管理",value ="/permission/centre/v1/blackorwhite")
@RequestMapping(value = "/permission/centre/v1/blackorwhite")
@Validated
public interface BlackOrWhiteApi {
    /**
     * 通用查询
     * @param blackOrWhiteQuery
     * @return
     */
    @ApiOperation(value = "查询黑白名单，分页")
    @GetMapping("")
    @ResponseBody
    List<BlackOrWhiteVO> commonQuery(@NotNull  BlackOrWhiteQuery blackOrWhiteQuery);
    /**
     *  批量更新
     * @param blackOrWhiteDTOS
     * @return true成功 false失败  如果一个失败则全部失败
     */
    @ApiOperation(value="批量更新")
    @PutMapping(value = "/batch")
    @ResponseBody
    Boolean batchUpdate(@NotNull @RequestBody List<BlackOrWhiteDTO> blackOrWhiteDTOS);

    /**
     * 增加一个黑白名单记录
     * @param: 黑白名单DTO
     * @return: 成功则true CommonResponse 应答码和消息统一参考基础框架
     */
    @ApiOperation(value = "增加黑白名单信息")
    @PostMapping(value = "")
    @ResponseBody
    Boolean add(@NotNull @Valid @RequestBody  BlackOrWhiteDTO blackOrWhiteDTO);

    /**
     *  删除用户选择的记录 采用乐观锁判定模式
     * @param id  记录id
     * @param version 记录版本
     * @return true成功 false失败
     */
    @ApiOperation(value = "删除黑白名单信息")
    @DeleteMapping(value = "")
    @ResponseBody
    Boolean remove(@NotNull @ApiParam(value = "当前行id", required = true) @RequestParam(value = "id", required = true) Long id,
                   @NotNull @ApiParam(value = "当前行版本", required = true) @RequestParam(value = "version", required = true) Long version);

    /**
     * 批量删除
     * @param blackOrWhiteDTOS
     * @return
     */
    @ApiOperation(value="批量删除黑白名单信息")
    @DeleteMapping(value="/batch")
    @ResponseBody
    Boolean batchDelete( @NotEmpty @RequestBody BlackOrWhiteDTO[] blackOrWhiteDTOS);

    /**
     * 修改黑白名单信息
     * @param blackOrWhiteDTO
     * @return true 成功 false失败
     */
    @ApiOperation(value = "修改黑白名单信息")
    @PutMapping(value = "")
    @ResponseBody
    Boolean modify(@Valid @RequestBody BlackOrWhiteDTO blackOrWhiteDTO);

    /**
     *  根据id查询黑白名单对象
     * @param id  黑名名单id
     * @return 黑名单对象
     */
    @ApiOperation(value = "通过id查询")
    @GetMapping(value = "{id}")
    @ResponseBody
    BlackOrWhiteVO queryById(@NotNull @PathVariable("id") Long id);

    /**
     * 批量擦汗如 批量插入黑名名单
     * @param blackOrWhiteDTOList
     * @return true成功 false失败
     */
    @PostMapping(value = "/batchadd")
    Boolean batchAdd(@NotNull @RequestBody List<BlackOrWhiteDTO> blackOrWhiteDTOList);

    @GetMapping(value = "/test")
    public BlackOrWhiteVO testRedis(BlackOrWhiteQuery blackOrWhiteQuery);
}
