package com.sd365.permission.centre.api;

import com.sd365.common.core.annotation.stuffer.CommonFieldStuffer;
import com.sd365.common.core.annotation.stuffer.MethodTypeEnum;
import com.sd365.permission.centre.pojo.dto.JobDTO;
import com.sd365.permission.centre.pojo.query.JobQuery;
import com.sd365.permission.centre.pojo.vo.JobVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 *
 */
@CrossOrigin
@Api(tags = "工作管理",value = "/permission/centre/v1/job")
@RequestMapping(value = "/permission/centre/v1/job")
public interface JobApi {
    /**
     * 通用查询
     * @param jobQuery
     * @return
     */
    @ApiOperation(value = "查询任务信息，分页")
    @GetMapping("")
    @ResponseBody
    @CommonFieldStuffer(methodType = MethodTypeEnum.QUERY)
    List<JobVO> commonQueryJob(JobQuery jobQuery);


    /**
     * @param: 任务DTO
     * @return: 成功则true CommonResponse 应答码和消息统一参考基础框架
     * @see
     * @since
     */
    @ApiOperation(value = "增加任务信息")
    @PostMapping(value = "/add")
    @ResponseBody
    Boolean add(@RequestBody @Valid JobDTO jobDTO);

    /**
     * @param:
     * @return:
     * @see
     * @since
     */
    @ApiOperation(value = "删除工作信息")
    @DeleteMapping("/delete")
    @ResponseBody
    Boolean remove(@ApiParam(value = "当前行id", required = true) @RequestParam(value = "id", required = true) Long id,
                   @ApiParam(value = "当前行版本", required = true) @RequestParam(value = "version", required = true) Long version,
                   @ApiParam(value = "当前行任务名", required = true) @RequestParam(value = "jobName", required = true) String jobName);

    /**
     * @return java.lang.Boolean
     * @Description 修改任务信息
     * @Param * @param jobDTO
     **/
    @ApiOperation(value = "修改任务信息")
    @PutMapping(value = "/modify")
    @ResponseBody
    Boolean modify(@Valid @RequestBody JobDTO jobDTO);

    /**
     * 暂停任务
     * @param jobDTO
     * @return
     */
    @ApiOperation(value = "暂停任务")
    @PutMapping(value = "/pauseJob")
    @ResponseBody
    Boolean pauseJob(@Valid @RequestBody JobDTO jobDTO);

    /**
     * 启动任务，按cron执行
     * @param jobDTO
     * @return
     */
    @ApiOperation(value = "启动任务")
    @PutMapping(value = "/resumeJob")
    @ResponseBody
    Boolean resume(@Valid @RequestBody JobDTO jobDTO);

    /**
     * 立即执行一次任务
     * @param jobDTO
     * @return
     */
    @ApiOperation(value = "立即执行一次任务")
    @PutMapping(value = "/runOnce")
    @ResponseBody
    Boolean runOnce(@Valid @RequestBody JobDTO jobDTO);

}
