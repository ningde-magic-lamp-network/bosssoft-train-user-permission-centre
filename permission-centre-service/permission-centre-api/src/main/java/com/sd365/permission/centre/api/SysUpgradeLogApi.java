package com.sd365.permission.centre.api;


import com.sd365.permission.centre.pojo.dto.SysUpgradeLogDTO;
import com.sd365.permission.centre.pojo.query.IdVersionQuery;
import com.sd365.permission.centre.pojo.query.SysUpgradeLogQuery;
import com.sd365.permission.centre.pojo.vo.SysUpgradeLogVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin
@Api(tags = "升级日志管理",value ="/permission/centre/v1/sysupgradelog")
@RequestMapping(value = "/permission/centre/v1/sysupgradelog")
public interface SysUpgradeLogApi {

    @ApiOperation(value="查询系统升级日志")
    @GetMapping(value = "")
    @ResponseBody
    List<SysUpgradeLogVO> commonSysUpgradeLogQuery(SysUpgradeLogQuery sysUpgradeLogQuery);

    @ApiOperation(value="查询某租户某子系统最新一篇升级日志")
    @GetMapping(value = "/latestOne")
    @ResponseBody
    SysUpgradeLogVO latestOne(@ApiParam(value = "租户id", required = true) @RequestParam("tenantId") Long tenantId,
                              @ApiParam(value = "子系统id", required = true) @RequestParam("subSystemId") Long subSystemId);

    @ApiOperation(value = "查询SysUpgradeLog通过ID")
    @GetMapping(value = "/{id}")
    @ResponseBody
    SysUpgradeLogVO queryById(@PathVariable("id") Long id);

    @ApiOperation(value = "更新日志")
    @PostMapping(value = "/add")
    @ResponseBody
    Boolean add(@ApiParam @RequestBody @Valid SysUpgradeLogDTO dto);

    @ApiOperation(value = "根据id和发行版本删除单条数据")
    @DeleteMapping(value = "/delete")
    @ResponseBody
    Boolean deleteOneById(@ApiParam(value = "当前行id", required = true) @RequestParam("id") Long id,
                          @ApiParam(value = "当前发行版本", required = true) @RequestParam("version") Long version);

    @ApiOperation(value = "根据id和发行版本list删除多条数据")
    @DeleteMapping(value = "/deleteBatch")
    @ResponseBody
    Boolean deleteBatch(@RequestBody List<IdVersionQuery> batchQuery);
}
