/**
 * Copyright (C), 2001-2022, www.bosssof.com.cn
 * FileName: ResourceApi
 * Author: Administrator
 * Date: 2022-9-27 17:23:18
 * Description: 文件定义了系统资源的管理接口，资源包括菜单 按钮  接口等
 * <p>
 * History:
 * <author> Administrator
 * <time> 2022-9-27 17:23:18
 * <version> 1.0.0
 * <desc> 添加重试重构代码的规范性
 */
package com.sd365.permission.centre.api;

import com.sd365.permission.centre.pojo.dto.ResourceDTO;
import com.sd365.permission.centre.pojo.query.ResourceQuery;
import com.sd365.permission.centre.pojo.vo.ResourceVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
/**
 * @Class ResourceApi
 * @Description 对系统的权限资源进行统一的管理，主要是包含资源的增删改查<br>
 *     资源将绑定到相关的角色上，对应后台表为resource表，存储资源角色关系表为role_resource表
 * @Author Administrator
 * @Date 2022-9-27  17:25
 * @version 1.0.0
 */
@CrossOrigin
@Api(tags = "资源管理 ",value ="/permission/centre/resource")
@RequestMapping(value = "/permission/centre/resource")
@Validated
public interface ResourceApi {

    /**
     *  系统初始化的时候将资源初始化到redis，系统定时任务会初始化和更新
     * @Author: Administrator
     * @DATE: 2022-9-27  19:42
     * @param: [resourceQuery]
     * @return: java.util.List<com.sd365.permission.centre.pojo.vo.ResourceVO>
     */
    @ApiOperation(value="redis初始化")
    @GetMapping(value = "/getRedis")
    @ResponseBody
    List<ResourceVO> initRedis(@NotNull ResourceQuery resourceQuery);

    /**
     * @Description: 一般指前端的页面的查询区传递符合查询参数对后端发起查询
     * @Author: Administrator
     * @DATE: 2022-9-27  19:47
     * @param: [resourceQuery]  查询条件 name,parent_id
     * @return: java.util.List<com.sd365.permission.centre.pojo.vo.ResourceVO>
     */
    @ApiOperation(value="资源模块查询")
    @GetMapping(value = "")
    @ResponseBody
    List<ResourceVO> commonQuery(@NotNull ResourceQuery resourceQuery );

    /**
     * @Description: 角色和部门模块用到此接口获取权限数
     * @Author: Administrator
     * @DATE: 2022-9-27  19:48
     * @param: [resourceQuery]
     * @return: java.util.List<com.sd365.permission.centre.pojo.vo.ResourceVO> 依赖前端对list进行递归遍历构成树节点
     */
    @ApiOperation(value="资源树查询")
    @GetMapping(value = "/tree")
    @ResponseBody
    List<ResourceVO> queryResourceTree(@NotNull ResourceQuery resourceQuery );

    /**
     * @param:  添加
     * @return:
     * @see
     * @since
     */

    @ApiOperation(value = "添加资源")
    @PostMapping(value = "")
    @ResponseBody
    /**
     * @Description: 增加资源
     * @Author: Administrator
     * @DATE: 2022-9-27  19:50
     * @param: [resourceDTO]  对应前端界面提交的资源参数，注意必填字段为空则触发参数校验异常
     * @return: java.lang.Boolean 添加成功返回true 但是返回值在应答切面会包装为标准CommonResponse
     */
    Boolean add ( @NotNull @Valid @RequestBody ResourceDTO resourceDTO );

    /**
     * 通过id获取资源对象
     * @param id 资源记录id
     * @return  资源对象
     */
    @GetMapping(value = "{id}")
    @ResponseBody
    ResourceVO queryResourceById(@NotNull @PathVariable("id") Long id);

    /**
     *  该方法应该要返回列表，检查前端发现也没有相应调用，因此备注该方法弃用
     * @param: parent id  父菜单id
     * @return:
     */
    @Deprecated
    @GetMapping(value = "findParent/{parentId}")
    @ResponseBody
    ResourceVO queryResourceByParentId(@PathVariable("parentId") Long id);

    /**
     * @Description: 提取子系统的资源,资源管理模块不使用此接口
     * @Author: Administrator
     * @DATE: 2022-9-28  10:43
     * @param: [] 虽然参数为空但是内部会依据租户提取
     * @return: java.util.List<com.sd365.permission.centre.pojo.vo.ResourceVO>
     */
    @Deprecated
    @GetMapping(value = "/menu")
    @ResponseBody
    List<ResourceVO> queryResourceByMenu();

    /**
     * @param:
     * @return:
     * @see
     * @since
     */
    @ApiOperation(value="删除资源")
    @DeleteMapping(value="")
    @ResponseBody
    Boolean remove(@ApiParam(value = "当前行id", required = true) @NotNull @RequestParam("id") Long id
//            ,@ApiParam(value = "当前行版本", required = true) @RequestParam("version") Long version
    );


    /**
     * @param: 围栏通知DTO数组
     * @return: 成功则true
     * @see
     * @since
     */
    @ApiOperation(value="批量删除资源")
    @DeleteMapping(value="/batch")
    @ResponseBody
    /**
     * @Description: 批量删除资源
     * @Author: Administrator
     * @DATE: 2022-9-27  18:24
     * @param: [resourceDTOS] 前端传递的对象数组采用对象数组是因为包含每个元素id和version
     * @return: java.lang.Boolean
     */
    Boolean batchDelete(@NotNull @Valid @RequestBody  ResourceDTO[] resourceDTOS);
    /**
     * @param:对资源进行修改
     * @return:成功 true
     * @see
     * @since
     */
    @ApiOperation(value="修改资源")
    @PutMapping(value = "")
    @ResponseBody
    Boolean modify(@NotNull @Valid @RequestBody ResourceDTO resourceDTO);


    /**
     * @Description: 前端不再调用该方法所以该方法可以不用实现
     * @Author: Administrator
     * @DATE: 2022-9-27  18:01
     * @param: [id] 被拷贝的记录id
     * @return: com.sd365.permission.centre.pojo.dto.ResourceDTO 当前资源对象
     */
    @ApiOperation(value="拷贝资源")
    @GetMapping(value = "/copy")
    @ResponseBody
    ResourceDTO copy(@NotNull Long id);

}
