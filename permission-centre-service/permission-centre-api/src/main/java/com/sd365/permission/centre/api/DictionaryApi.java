/**
 * Copyright (C), -2025, www.bosssof.com.cn
 * @FileName DictionaryApi.java
 * @Author Administrator
 * @Date 2023-02-22  11:44
 * @Description 系统的字段管理接口的维护，存储了系统的所有的字段数据
 * History:
 * <author> Administrator
 * <time> 2023-02-22  11:44
 * <version> 1.0.0
 * <desc> 初始版本，目前只有简单的增删改查功能
 */
package com.sd365.permission.centre.api;

import com.sd365.common.log.api.annotation.ApiLog;
import com.sd365.permission.centre.pojo.dto.DictionaryDTO;
import com.sd365.permission.centre.pojo.query.DictionaryQuery;
import com.sd365.permission.centre.pojo.vo.DictionaryVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
/**
 * @Class DictionaryApi
 * @Description 字段管理的接口类，提供前端vue界面的增删改查调用
 * @Author Administrator
 * @Date 2023-02-22  11:45
 * @version 1.0.0
 */
@CrossOrigin
@Api(tags = "字典管理",value = "permission/centre/v1/dictionary")
@RequestMapping(value = "/permission/centre/v1/dictionary")
@Validated
public interface DictionaryApi {
    /**
     * @param: 字典DTO
     * @return: 成功则true CommonResponse 应答码和消息统一参考基础框架
     * @see
     * @since
     */

    @ApiOperation(value="增加字典")
    @PostMapping(value="")
    @ResponseBody
    Boolean add(@NotNull @Valid @RequestBody DictionaryDTO dictionaryDTO);

    /**
     *  删除选择行
     * @param id  行id
     * @param version  行的版本id 每次更新版本都新增，引入版本是为了解决并发删除问题
     * @return true代表成功 false失败
     */
    @ApiOperation(value="删除字典")
    @DeleteMapping(value="")
    @ResponseBody
    Boolean remove(@ApiParam(value = "当前行id", required = true) @NotNull @RequestParam("id") Long id,
                   @ApiParam(value = "当前行版本", required = true) @NotNull @RequestParam("version") Long version);


    /**
     *  修改某一个字段数据
     * @param dictionaryDTO
     * @return
     * @throws Exception
     */
    @ApiOperation(value="修改字典")
    @PutMapping(value = "")
    @ResponseBody
    Boolean modify(@NotNull @Valid @RequestBody DictionaryDTO dictionaryDTO);

    /**
     * 批量修改字典
     * @param dictionaryDTOS 批量更新 主要用于状态更新
     * @return
     */
    @ApiOperation(value="批量修改字典")
    @PutMapping(value = "/batch")
    @ResponseBody
    Boolean batchUpdate( @NotNull @RequestBody DictionaryDTO[] dictionaryDTOS) ;

    @ApiOperation(value="批量删除字典")
    @DeleteMapping(value = "/batch")
    @ResponseBody
    Boolean batchRemove( @NotNull @RequestBody DictionaryDTO[] dictionaryDTOS) ;

    /**
     * @param: 被拷贝的记录id
     * @return: 拷贝后的完整对象
     * @see
     * @since
     */
    @ApiOperation(value="拷贝字典")
    @PostMapping(value = "/copy")
    @ResponseBody
    DictionaryDTO copy(@NotNull  Long id);

    /**
     *      系统框架GlobalControllerResolver 类分析 参数是否 BaseQuery 类型 ，如果是则 拦截调用
     *      <br> PageHelper 分页方法， 并且将返回的page对象放入TheadLocal ，方法返回参数被 ResponseBodyAware拦截
     *       <br> 其判断 返回值的类型 如果是属于分页的请求则 自动将 List<DictionaryVO> 装入CommonPage
     *       <br> 并且构建统一应答回去 以上改进优化了 请求和应答的方法的编写
     * @param dictionaryQuery  查询区提交的查询条件组合
     * @return 字段列表
     */
    @ApiOperation(value="查询字典")
    @GetMapping(value = "")
    @ApiLog
    @ResponseBody
    List<DictionaryVO> commonQuery(@NotNull DictionaryQuery dictionaryQuery);

    /**
     *  通过id查询返回对象
     * @param id  记录id
     * @return  查找到对象转化为VO 用于界面显示
     */
    @ApiOperation(value="查询字典通过ID")
    @GetMapping(value = "{id}")
    @ResponseBody
    DictionaryVO queryDictionaryById(@NotNull @PathVariable("id") Long id);

    /**
     *  根据字段类型查询字段内容
     * @param id  字典类型id
     * @return  字典数据列表
     */
    @ApiOperation(value="查询字典通过类型ID")
    @GetMapping(value = "/classify/{id}")
    @ResponseBody
    List<DictionaryVO> queryDictionaryByTypeId(@NotNull @PathVariable("id") Long id);

}
