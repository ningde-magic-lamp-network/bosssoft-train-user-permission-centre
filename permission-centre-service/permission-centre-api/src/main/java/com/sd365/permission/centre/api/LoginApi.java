/**
 * Copyright (C), 2001-2022, www.bosssof.com.cn
 * FileName: LoginApi
 * Author: Administrator
 * Date: 2022-10-8 18:07:44
 * Description:
 * <p> 重构了LoginController接口 新增加了接口给LoginController实现重构了RequestMapping URL
 * History:
 * <author> Administrator
 * <time> 2022-10-8 18:07:44
 * <version> 1.0.0
 * <desc> 版本描述
 */
package com.sd365.permission.centre.api;

import com.sd365.common.log.api.annotation.ApiLog;
import com.sd365.permission.centre.pojo.dto.LoginUserDTO;
import com.sd365.permission.centre.pojo.vo.UserVO;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;

/**
 *  LoginApi
 *  控制器实现此接口，该接口主要是提供登录服务，主要的调用者来自网关
 * @Author: Administrator
 * @Date: 2022-10-08 18:07
 **/
@RequestMapping(value = "/permission/centre/v1")
@CrossOrigin
public interface LoginApi {
    /**
     * 控制层不应该包括业务逻辑，该方法需要被优化，多数的业务逻辑迁移到LoginService 类
     *   前端发起的 permission/centre/v1/user/login 但是在请求在 网关配置被转发到 认证
     * - id: gateway-authentication-service
     *  uri: lb://gateway-authentication
     *  predicates:
     *- Path=/permission/centre/v1/user/login
     * filters:
     * - RewritePath=/permission/centre/v1/user/login, /auth/token
     * abel.zhan 2024-10-22 重构了该方法，替换返回值 UserVO为 LoginUserDTO，内部认证异常则抛出
     * 这些修改将导致前端大量修改相关数据结构，但是总体上程序的接口定义更加合理了
     * @param: 认证服务传递过来的 账号，密码，租户号
     * @return: 认证成功的用户信息内部包含数据 ，认证失败则返回对象但是对象内部无数据，前端需要依据UserVO内部数据判断
     */
    @ApiLog
    @GetMapping(value = "/user/auth")
    @ResponseBody
    LoginUserDTO auth(@NotBlank  @RequestParam(value = "code") String code,
                      @NotBlank @RequestParam(value = "password") String password,
                      @NotBlank @RequestParam(value = "account") String account);

        /**
         *  该方法可能作废
         * @param:
         * @return:
         */
        @Deprecated
        @ApiLog
        @GetMapping(value = "/user/login")
        @ResponseBody
    public LoginUserDTO login(@NotBlank @RequestParam(value = "code") String code,
                              @NotBlank @RequestParam(value = "password") String password,
                              @NotBlank @RequestParam(value = "tenant") String tenant);
    /**
     * 前端认证完成后调用此方法获取用户信息，用户信息包含权限信息
     * abel.zhan 2024-10-22日重构该业务，移除了返回值loginApi.UserVO，改用pojo/vo/UserVO
     * userVO 包含角色VO 和 角色VO说包含的资源VO
     * @param: code 工号，
     * @param  tenantAccount  租户账号
     * @return : UserVO 包含角色和资源信息
     */
    @ApiLog
    @GetMapping(value = "/user/info")
    @ResponseBody
    UserVO info(@NotBlank @RequestParam(value = "code") String code, @NotBlank @RequestParam(value = "account") String tenantAccount);

    /**
     * 原业务逻辑写在controller中后改为调用UserOnlineService
     *
     * @return: 返回登录用户的id
     */
    @ApiLog
    @GetMapping(value = "/user/logout")
    String logout();
}
